# LCMTEST10

#### Description
* LCM test board using stm32 and RA8876


#### SDcard folder Structure
* Hardware: LCMTEST10_rev7+
* Structure tree view:
```bash
	SD root
	├─LCM_PROD_1
	│  ├─IMG_ASM
	│  ├─IMG_COG
	│  └─IMG_DISP
	├─LCM_PROD_0
	│  ├─IMG_ASM
	│  ├─IMG_COG
	│  └─IMG_DISP
	├─LCM_PROD_3
	│  ├─IMG_ASM
	│  ├─IMG_COG
	│  └─IMG_DISP
	└─LCM_PROD_2
	   ├─IMG_ASM
	   ├─IMG_COG
	   └─IMG_DISP
```
* In every IMG_xxxx folder, there should be a <main.lua>, and some image files, max of image files count is 32.

#### User Key
* Key and Switch position
```bash
        --------------------------
        |                        |
        |                        |
        |                        |
        |                        |
        |                        |
    SW0	|---|                    |
    SW1	|   |                    |
    SW2	|   |                    |
    SW3	|---|                    |
        |                      --|
        |                      | | key1
        |                      | | key0
        |                      --|
        --------------------------
```
* Key function
	* key0:
		* When board startup, press and power on , board will go into USB mode, work as a USB-Disk
		* When normal working, will pluse the image looping.
	* key1: __RESERVERED__
	* SW0, SW1: product folder selection:
		* SW0 = 0, SW1 = 0 : LCM_PROD_0
		* SW0 = 0, SW1 = 1 : LCM_PROD_1
		* SW0 = 1, SW1 = 0 : LCM_PROD_2
		* SW0 = 1, SW1 = 1 : LCM_PROD_3
	* SW2, SW3: testing type selection:
		* SW2 = 0, SW2 = 0 : IMG_DISP
		* SW2 = 0, SW2 = 1 : IMG_ASM
		* SW2 = 1, SW2 = 0 : IMG_COG
		* SW2 = 1, SW2 = 1 : __RESERVERED__




