function test_display(f_write, f_read)
    --writeReg(3, 0xea, 0x85, 0x55)
    state = f_read(0x0A,1)
    print(string.format('state is %02x', state))
    
    --writeReg(1, 0x10)
    f_write(1)
    DsiD(0x10)
    state = f_read(0x0A,1)
    print(string.format('state is %02x', state))
    delay_ms(200)
    
    --writeReg(1, 0x11)
    f_write(1)
    DsiD(0x11)
    state = f_read(0x0A,1)
    print(string.format('state is %02x', state))
    delay_ms(200)
    
    --writeReg(1, 0x28)
    f_write(1)
    DsiD(0x28)
    state = f_read(0x0A,1)
    print(string.format('state is %02x', state))
    delay_ms(200)
    
    --writeReg(1, 0x29)
    f_write(1)
    DsiD(0x29)
    state = f_read(0x0A,1)
    print(string.format('state is %02x', state))
end


test_display(DcsS, DcsR)

test_display(GenS, GenR)