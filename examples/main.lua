--[[ bsp examples ]]
	t10_autoPower(0) --0: run forever. 1:run once
	t10_setInterval(500) -- set image display time interval

	-- GPIO usage, mode could be OUTPUT_PP OUTPUT_OD INPUT, pp setting could be NOPULL, PULLUP, PULLDOWN
	ioInitial(2, "OUTPUT_PP", "NOPULL")
	ioInitial(3, "OUTPUT_PP", "NOPULL")
	ioInitial(4, "OUTPUT_PP", "NOPULL")
	ioInitial(5, "OUTPUT_PP", "NOPULL")
	function rst(state)
		ioWrite(2, state)
	end
	
	-- DEPRECATED! IO@45pin, 2,3,4,5, OUTPUTPP mode usage
	-- DEPRECATED! io4(1)
	-- DEPRECATED! delay_ms(100)
	-- DEPRECATED! io4(0)
	-- DEPRECATED! delay_ms(1000)
	-- DEPRECATED! io4(1)
	-- DEPRECATED! delay_ms(100)

--[[ util function examples ]]
	delay_ms(100) -- delay by milliseconds
	
	--print on LCD after Lcd initial finished
	p1:x
	p2:y
	p3:size
	p4:front color
	p5:background color
	p6:string to print
	t10_print(0, 0, 0, 0xff0000, 0x0000ff, "OPT3002TEST")


--[[ interface examples ]]
	ei2c_init(2, 3, 0x5A, 100) -- initial emulate I2c (sdapin, sclpin, slave address, frequence)
	
	-- ei2c write data
	function i2cw(reg, dat)
		ei2c_write1Byte(reg, dat)
	end

	-- ei2c read data
	local id1 = ei2c_read1Byte(0x00)
	local id2 = ei2c_read1Byte(0x01)
	local id3 = ei2c_read1Byte(0x02)

	-- emulate a SPI write command 
	function write_com(byte)
		dc(0)
		for i = 0, 7, 1 do
			scl(0)
			sda(byte & 0x80)
			byte = byte << 1
			scl(1)
		end
	end
	
	-- emulate a SPI write data
	function write_dat(byte)
		dc(1)
		for i = 0, 7, 1 do
			scl(0)
			sda(byte & 0x80)
			byte = byte << 1
			scl(1)
		end
	end

	-- spi write variable number of data
	function writeReg(...)
		local s = 0
		for i, v in ipairs{...} do   --> {...} 表示一个由所有变长参数构成的数组
			if(i == 1) --lua里, 数组是从1开始的!
			then
			write_com(v)
			else
			write_dat(v)
			end
		end
		return s
	end

	-- st7701s
	function lua_cmd_3spi_9bcs(par)
		local i=0
		local b = par
		
		my_cs(0)
		my_sck(0)
		my_sda(0)
	
		my_sck(1) 
	
		for i=0,7,1 do
		
			my_sck(0) 
			if (b & 0x80 ~= 0) then
				my_sda(1)
			else
				my_sda(0)
			end
			my_sck(1)
			b=b<<1	   
		end
		my_cs(1)
	end
	
	-- st7701s
	function lua_dat_3spi_9bcs(par)
		local i=0
		local b = par
		
		my_cs(0)
		my_sck(0)
		my_sda(1)
	
		my_sck(1) 
	
		for i=0,7,1 do
		
			my_sck(0) 
			if (b & 0x80 ~= 0) then
				my_sda(1)
			else
				my_sda(0)
			end
			my_sck(1)
			b=b<<1	   
		end
		my_cs(1)
	end
	
	-- st7701s
	function writeReg(...)
		local s = 0
		for i, v in ipairs{...} do   --> {...} 表示一个由所有变长参数构成的数组
			--s = s + v
			--print('i '..i..' v '..v..' s\r\n')
			if(i == 1) then
				wr_cmd_3spi_9bcs(v)
			else
				wr_dat_3spi_9bcs(v)
			end
		end
		return s
	end

--[[ MIPI-DBI examples ]]
	t10_DPI(800, 100, 30, 30, 480, 18, 3, 3, 1,1,0,1, 70) -- DPI timing

--[[ MIPI-DPI examples ]]

--[[ MIPI-DSI examples ]]
	t10_DSI(1024,64,160,160, 600,8,23,12, 1,0,0,1, 50, 4, 0)  -- DPI timing 
	-- + lane number(1-4) 
	-- + mode(0:sync pause, 1:sync event, 2:...)

	GenS(1) -- generic short write n bytes

	DsiD(0x11) -- packet data

	DcsS(1) --DCS short write n bytes
	DsiD(0x29)

	checkMipiIdDcs() -- check LCD Drive 0x04 register by DCS
	checkMipiIdGen() -- check LCD Drive 0x04 register by GEN

	MIPI_VIDEO() -- enter video mode

	-- ST7701S MIPI DcS Long Packet
	function writeReg(...)
		local s = 0
		len = #{...}
		print(string.format("code len: %d\r\n", len))
	
		if (len <= 2) then
		-- GenS(v)
		DcsS(len)
		else
		-- GenL(v)
		DcsL(len)
		end
	
		for i, v in ipairs {...} do
			DsiD(v)
		end
	
		return s
	end

--[[ LUA examples ]]
	-- garbage collection
	collectgarbage("collect")
		-- do something
	collectgarbage("stop")

	-- string format
	print(string.format("LT9211 Chip ID: %02x %02x %02x \r\n", id1, id2, id3))
	print("lua print function")

	-- lua 2D table
	ILI9881=
	{
	{0xFF,0x98,0x81,0x03},
	{0x01,0x00},
	{0x02,0x00},
	{0x03,0x73},
	{0x04,0x06},
	{0x05,0x00},}

	-- lua for loop
	for i=1,Len,1 do
		i2cw( 0x03, t[i] )
	end

	-- function recive variable number of input
	function add(...)
		local s = 0
		for i, v in ipairs{...} do   --> {...} 表示一个由所有变长参数构成的数组
			s = s + v
		end
		return s
	end
	print(add(3,4,5,6,7))

	function gen2b(reg, value)
		GenL(2)
		DsiD(reg)
		DsiD(value)
	end

	-- 
	function writeReg(...)
		local s = 0
		for i, v in ipairs{...} do   --> {...} 表示一个由所有变长参数构成的数组
			if(i == 1)
			then
			--GenL(v)
				if(v<=3)
				then
					GenS(v)
				else
					GenL(v)
				end
				else
					DsiD(v)
			end
		end
		return s
	end
	