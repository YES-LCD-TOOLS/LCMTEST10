PROG_NAME = "YTCA10TLAJ-09-100N-V2"
t10_autoPower(0)
function EK9713()
    HA=800
    HSW=2
    HBP=46-HSW
    HFP=210
    VA=480
    VSW=4
    VBP=23-VSW
    VFP=22
    FPS=60
    HSYNC_POL=1     --0:LOW-ACT 1:HIGH-ACT
    VSYNC_POL=1     --0:LOW-ACT 1:HIGH-ACT
    DE_POL=0        --0:HIGH-ACT 1:LOW-ACT
    CLK_POL=1       --0:RISING-EDGE-LATCH 1:FALLING-EDGE-LATCH
end

function HX8282()
    HA=1024
    HSW=20
    HBP=160-HSW
    HFP=160
    VA=600
    VSW=8
    VBP=23-VSW
    VFP=12
    FPS=60
    HSYNC_POL=1     --0:LOW-ACT 1:HIGH-ACT
    VSYNC_POL=0     --0:LOW-ACT 1:HIGH-ACT
    DE_POL=0        --0:HIGH-ACT 1:LOW-ACT
    CLK_POL=1       --0:RISING-EDGE-LATCH 1:FALLING-EDGE-LATCH
end


Defult = {
    --              0       1       2       3       4       5       6       7       8       9       a       b       c       d       e       f
    --[[00]]        0x00,   0xff,   0xff,   0xff,   0xff,   0xff,   0xff,   0x00,   0x4e,   0x81,   0xf4,   0x03,   0x00,   0x00,   0x00,   0x00,    
    --[[10]]        0x1a,   0x15,   0x01,   0x03,   0x80,   0x16,   0x0d,   0x78,   0x0a,   0xbb,   0xd0,   0x94,   0x57,   0x55,   0x91,   0x27, 
    --[[20]]        0x21,   0x50,   0x54,   0x00,   0x00,   0x00,   0x01,   0x01,   0x01,   0x01,   0x01,   0x01,   0x01,   0x01,   0x01,   0x01,     
    --[[30]]        0x01,   0x01,   0x01,   0x01,   0x01,   0x01,   0xb0,   0x13,   0x00,   0x40,   0x41,   0x58,   0x19,   0x20,   0x30,   0x20,     
    --[[40]]        0x3a,   0x00,   0xdf,   0x7d,   0x00,   0x00,   0x00,   0x19,   0x00,   0x00,   0x00,   0x0f,   0x00,   0x00,   0x00,   0x00,     
    --[[50]]        0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x01,   0x00,   0x00,   0xfe,   0x00,   0x53,     
    --[[60]]        0x54,   0x41,   0x52,   0x52,   0x59,   0x0a,   0x20,   0x20,   0x20,   0x20,   0x20,   0x20,   0x00,   0x00,   0x00,   0xfe,     
    --[[70]]        0x00,   0x4b,   0x52,   0x31,   0x30,   0x31,   0x4c,   0x42,   0x36,   0x53,   0x20,   0x41,   0x20,   0x0a,   0x00,   0x30,     
}
    
YTCA10TLAJ_09_100N_V2={
--	0	1	2	3	4	5	6	7	8	9	A	B	C	D	E	F
--[[00]] 	0x00,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0x00,	0x64,	0xB3,	0xF4,	0x03,	0x00,	0x00,	0x00,	0x00,
--[[10]] 	0x19,	0x20,	0x01,	0x03,	0x80,	0x16,	0x0D,	0x78,	0x0A,	0x52,	0x5A,	0x94,	0x59,	0x5B,	0x95,	0x27,
--[[20]] 	0x1B,	0x4B,	0x50,	0x00,	0x00,	0x00,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,
--[[30]] 	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0xC0,	0x12,	0x00,	0x40,	0x41,	0x58,	0x23,	0x20,	0xA0,	0x10,
--[[40]] 	0xC8,	0x00,	0xDF,	0x7D,	0x00,	0x00,	0x00,	0x18,	0x00,	0x00,	0x00,	0x0F,	0x00,	0x00,	0x00,	0x00,
--[[50]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x01,	0x00,	0x00,	0xFE,	0x00,	0x53,
--[[60]] 	0x54,	0x41,	0x52,	0x52,	0x59,	0x0A,	0x20,	0x20,	0x20,	0x20,	0x20,	0x20,	0x00,	0x00,	0x00,	0xFE,
--[[70]] 	0x00,	0x4B,	0x52,	0x31,	0x30,	0x31,	0x4C,	0x42,	0x36,	0x53,	0x20,	0x41,	0x20,	0x0A,	0x01,	0xB9,
--[[80]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
--[[90]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
--[[A0]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
--[[B0]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
--[[C0]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
--[[D0]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
--[[E0]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
--[[F0]] 	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,
   nil,															
}


local GT9X_WADD = 0xA0
local GT9X_RADD = 0xA1

local nWR = 2
local SDA = 4
local SCL = 5
function i2c_initial()
	ioInitial(SDA, "OUTPUT_OD", "NOPULL")
	ioInitial(SCL, "OUTPUT_OD", "NOPULL")
    ioInitial(nWR, "OUTPUT_OD", "NOPULL")
end

function sda_in()
	ioInitial(SDA, "INPUT", "NOPULL")
end

function sda_out()
	ioInitial(SDA, "OUTPUT_OD", "NOPULL")
end

function sda_set(state)
	if state ~= 0 then
		ioWrite(SDA, 1)
	else 
		ioWrite(SDA, 0)
	end
end

function scl_set(state)
--[[	if state ~= 0 then
		ioWrite(SCL, 1)
	else 
		ioWrite(SCL, 0)
	end]]
    ioWrite(SCL,state)
end

function nWR_set(state)
    ioWrite(nWR,state)
end

function sda_get()
	local state = ioRead(SDA)
	if state ~= 0 then 
		return 1
	else 
		return 0
	end
end

function i2c_start()

	sda_out()

	sda_set(1)
	scl_set(1)
	delay_us(5)
	sda_set(0)
end	

function i2c_stop()
	sda_out()

	sda_set(0)
	scl_set(1)
	delay_us(5)
	sda_set(1)
end

function i2c_ack()

	scl_set(0)	
	
	sda_out()
	--send ACK signal */
	sda_set(0)	  
	delay_us(5)
	scl_set(1)	
	delay_us(5)
	-- the 9th clock start */
	
	-- SCL hold low to wait */
	scl_set(0)
	sda_set(1)

end

function i2c_nack()

	scl_set(0)
	--send NACK signal */
	sda_out()
	sda_set(1)	
	delay_us(5)
	scl_set(1)	
	delay_us(5)
	-- the 9th clock start */
	-- SCL hold low to wait */
	scl_set(0)	
	sda_set(1)
end

-- 1 ok, 0 time out
function i2c_ReadACK()

	local ret = 0
	local time_out = 0

	scl_set(0)

	sda_in()
	delay_us(4)

	scl_set(1)
	delay_us(4)

	while time_out < 50 do 
		ret = sda_get()
		if ret == 0 then 
			break
		end 
		
		time_out = time_out + 1

		delay_us(4)
	end

	sda_out()
	scl_set(0)
	delay_us(4)
	
	sda_set(1)

	if time_out < 50 then 
		return 1
	else 
		return 0
	end
end

function i2c_wbyte(bd)
	local bit_Cnt
	
	sda_out()

	for bit_Cnt=0,7,1 do
		scl_set(0)
		delay_us(3)

		if ((bd<<bit_Cnt) & 0x80) ~= 0 then
			sda_set(1)
		else 
			sda_set(0)
		end
		delay_us(3)
		
		scl_set(1)
		delay_us(4)
	end

	scl_set(0)
	delay_us(3)	
end

function i2c_rbyte()
	local retc
	local bit_Cnt
	local retc=0 
	--sda_set(1)
	sda_in()
	for bit_Cnt=0,7,1 do
		scl_set(0)
		delay_us(4)

		scl_set(1)			
		
		retc=retc<<1
		if (sda_get() ~= 0) then 
			retc = retc + 1
		end					
		delay_us(4)
	end

	scl_set(0)	
	delay_us(5)

	return   retc
end

function WriteI2C(regadd, command)
	--print(string.format("%02X", regadd ))
	--print(#args)
	--print(args)
	i2c_start()
	i2c_wbyte(GT9X_WADD)
	i2c_ReadACK()
	i2c_wbyte(regadd & 0xff)
	i2c_ReadACK()
	i2c_wbyte(command)
	i2c_ReadACK()
	i2c_stop()
end

function ReadI2C(regadd)
	i2c_start()
	i2c_wbyte(GT9X_WADD)
	i2c_ReadACK()
--	i2c_wbyte((regadd >> 8) & 0xff)
--	i2c_ReadACK()
	i2c_wbyte(regadd & 0xff)
	i2c_ReadACK()
	i2c_stop()

 
	i2c_start()
	i2c_wbyte(GT9X_RADD)
	i2c_ReadACK()
	local data = i2c_rbyte()
	i2c_nack()
	i2c_stop()
	return data
end

function Write_read(addr,data)
    WriteI2C(addr,data)
    local dat=ReadI2C(addr)
    print(string.format("read %02x is %02x should be %02x",addr , dat , data ))
    delay_ms(100) 
end

function Readonly(addr)
    local dat=ReadI2C(addr)
    print(string.format("read %02x is %02x",addr, dat)) 
end

function Write00()
    print("------------")
    Write_read(0x00,0x00)
end


function Write_read_I2C(initarray)
    for i,v in ipairs(initarray) do
        WriteI2C(i-1,v)
        delay_ms(10) 
    end
    --sda_set(0)
    --scl_set(0)

    delay_ms(1000)
end

function NVM(initarray)
    if (CheckReg(initarray) ~= 0) then
        HX8282()
        --EK9713()
        t10_DPI(HA,HSW,HBP,HFP,VA,VSW,VBP,VFP, HSYNC_POL,VSYNC_POL,DE_POL,CLK_POL, FPS)
        fillColor(0xff, 0xff, 0xff);
		t10_print_ex(0,0,24, 0x00, 0xFFFFff, PROG_NAME, 2,2,0,0)
		t10_print_ex(0,64,24, 0x00, 0xFFFFff, "MTP START", 2,2,0,0)

        

        nWR_set(1)
        --Readonly(0x00)
        nWR_set(0)
        delay_ms(1000)
        Write_read_I2C(initarray)
        --Write00()
        nWR_set(1)
        if (CheckReg(initarray) ~= 0) then
            t10_DPI(HA,HSW,HBP,HFP,VA,VSW,VBP,VFP, HSYNC_POL,VSYNC_POL,DE_POL,CLK_POL, FPS)
            fillColor(0xff, 0x00, 0x00);
            t10_print_ex(0,0,24, 0x00, 0xFFFFff, PROG_NAME, 2,2,0,0)
            t10_print_ex(0,64,24, 0x00, 0xFFFFff, "MTP FAILED", 2,2,0,0)
            while 1 do
                delay_ms(1000)
            end
        else
            print("[MTP] Success")
            --CheckReg(YTCA10TLAJ_09_100N_V2)
        end
    else
        print("[MTP] Already Done, Do nothing")
        HX8282()
        t10_DPI(HA,HSW,HBP,HFP,VA,VSW,VBP,VFP, HSYNC_POL,VSYNC_POL,DE_POL,CLK_POL, FPS)
		fillColor(0x00, 0xff, 0x00);
		t10_print_ex(0,0,24, 0x00, 0xFFFFff, PROG_NAME, 2,2,0,0)
		t10_print_ex(0,64,24, 0x00, 0xFFFFff, "MTP Check OK", 2,2,0,0)
		delay_ms(2000)
		
    end
end

function Readall(initarray)
    print("------------")
    for i,v in ipairs(initarray) do
        Readonly(i-1)
    end
end

function CheckReg(initarray)
    print("------------")
    local diff = 0
    local reg = 0xff
    for i=0,0xff,1 do
        reg = ReadI2C(i)
        if (reg ~= initarray[i+1]) then
            print("[CheckReg] Find Diff")
            print(string.format("add: %02x = %02x , should be: %02x ",i, reg, initarray[i+1]))
            diff = 1
            --break
        else
            print(string.format("read %02x is %02x",i, reg)) 
        end
    end
    return diff
end

PwmOn(14,1000,100)   --2021.03.31 add PWM output
i2c_initial()
NVM(YTCA10TLAJ_09_100N_V2)


HX8282()
--EK9713()
t10_DPI(HA,HSW,HBP,HFP,VA,VSW,VBP,VFP, HSYNC_POL,VSYNC_POL,DE_POL,CLK_POL, FPS)


t10_setInterval(800)