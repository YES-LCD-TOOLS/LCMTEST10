#include "bsp_luaep.h"
#include "bsp.h"

int lua_BSP_PwmOn(lua_State* L)
{
	int adu45PinNum = lua_tointeger(L, 1);
	int freq = lua_tointeger(L, 2);
	int duty = lua_tointeger(L, 3);

	BSP_PwmOn(adu45PinNum, freq, duty);
	printf("PWM %d pin start: %dHz, %d%%duty\r\n", adu45PinNum, freq, duty);
	return 0;
}

int lua_BSP_PwmOff(lua_State* L)
{
	int adu45PinNum = lua_tointeger(L, 1);
	BSP_PwmOff(adu45PinNum);
	return 0;
}


//bsp function export
struct luaL_Reg functionExport_bsp[] = {
	{"PwmOn", 	lua_BSP_PwmOn},
	{"PwmOff", 	lua_BSP_PwmOff},
	{NULL, NULL}
};
