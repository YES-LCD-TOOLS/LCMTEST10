#ifndef __BSP_H__
#define __BSP_H__

#include "main.h"
#include <stdint.h>

struct BSP_GPIO {
	GPIO_TypeDef *port;
	uint16_t pin;
	int dir;   // 0:in, 1:out-pp, 2:out-od
	int pull;  // 0:no pull, 1:pullup, -1:pulldown
	int inuse; // 0:not in use, 1:in use
};

uint16_t BSP_getSTM32PinNmb(uint8_t luaPinNum);
GPIO_TypeDef *BSP_getSTM32PortNmb(uint8_t luaPinNum);

uint32_t BSP_WaitKeyPress(uint8_t keyNum);
int BSP_getKeyState(int keyNumber);

void BSP_PwmDefaultInitial(void);
uint32_t BSP_PwmOn(uint8_t adu45PinNum, uint32_t freq, uint8_t duty);
uint32_t BSP_PwmOff(uint8_t adu45PinNum);
void _BSP_TIM_Init(uint8_t adu45PinNum, uint32_t freq, uint8_t duty);
void _BSP_TIM_MspPostInit(uint8_t adu45PinNum);

int calculatePwmInaccuracy_UnitTest(void);
int findBestPwm_UnitTest(void);

#endif //__BSP_H__