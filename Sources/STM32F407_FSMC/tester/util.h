#ifndef __UTIL_H__
#define __UTIL_H__

#include "main.h"

#define u32 unsigned int
#define u16 unsigned short
#define u8 unsigned char

#define TRUE 1
#define FALSE 0

#define CHECK_ID 0

#define DIV_ROUND_UP(n, d) (((n) + (d) - 1) / (d))

extern void sortStrings(char *strings[], int num);
extern void getUid(uint32_t *uidbuf);
extern int checkUid(void);
extern int tolower(int c);
extern int htoi(char s[]);
extern int compareVersion(const char *v1, const char *v2);
int int_pow(int n, int m);
void mdelay(u32 time);
void udelay(u32 time);

#endif //__UTIL_H__
