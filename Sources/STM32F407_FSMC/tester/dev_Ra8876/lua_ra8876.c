#include "lua_ra8876.h"

#include "RA8876_API.h"

#include "RA8876.h"

#include "main.h"

#include <stdint.h>

//void RA8876_MPU8_8bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);


//void RA8876_MPU8_16bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);


//void RA8876_MPU8_24bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);


//void RA8876_MPU16_16bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);


//void RA8876_MPU16_24bpp_Mode1_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);


//void RA8876_MPU16_24bpp_Mode2_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);


//void RA8876_PIP(uint8_t On_Off, uint8_t Select_PIP, uint32_t PAddr, uint16_t XP, uint16_t YP, uint32_t ImageWidth, uint16_t X_Dis, uint16_t Y_Dis, uint16_t X_W, uint16_t Y_H);


//void RA8876_Print_Internal_Font_Hexvariable(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, unsigned int tmp2);


//void RA8876_Print_Internal_Font_Decimalvariable(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, unsigned int tmp2);


//void RA8876_Print_Internal_Font_String(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char tmp2[]);


//void RA8876_Print_BIG5String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char *tmp2);


//void RA8876_Print_GB2312String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char *tmp2);


//void RA8876_Print_GB12345String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char tmp2[]);


//void RA8876_Print_UnicodeString(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, uint16_t *tmp2);


//void RA8876_Select_Font_Height_WxN_HxN_ChromaKey_Alignment(uint8_t Font_Height, uint8_t XxN, uint8_t YxN, uint8_t ChromaKey, uint8_t Alignment);


//void RA8876_Show_String(char *str);


//void RA8876_Draw_Line(uint32_t LineColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);


//void RA8876_Draw_Triangle(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X3, uint16_t Y3);


//void RA8876_Draw_Triangle_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X3, uint16_t Y3);


//void RA8876_Draw_Circle(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t R);


//void RA8876_Draw_Circle_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t R);


//void RA8876_Draw_Ellipse(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Ellipse_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Left_Up_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Left_Up_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Right_Down_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Right_Down_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Right_Up_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Right_Up_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Left_Down_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Left_Down_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Square(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);
static int lua_RA8876_Draw_Square(lua_State *L)
{
	uint32_t ForegroundColor = lua_tointeger(L, 1);
	uint32_t X1 = lua_tointeger(L, 2);
	uint32_t Y1 = lua_tointeger(L, 3);
	uint32_t X2 = lua_tointeger(L, 4);
	uint32_t Y2 = lua_tointeger(L, 5);
	RA8876_Draw_Square(ForegroundColor, X1, Y1, X2, Y2);
	return 0;
}

//void RA8876_Draw_Square_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);
static int lua_RA8876_Draw_Square_Fill(lua_State *L)
{
	uint32_t ForegroundColor = lua_tointeger(L, 1);
	uint32_t X1 = lua_tointeger(L, 2);
	uint32_t Y1 = lua_tointeger(L, 3);
	uint32_t X2 = lua_tointeger(L, 4);
	uint32_t Y2 = lua_tointeger(L, 5);
	RA8876_Draw_Square_Fill(ForegroundColor, X1, Y1, X2, Y2);
	return 0;
}

//void RA8876_Draw_Circle_Square(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t R, uint16_t Y_R);


//void RA8876_Draw_Circle_Square_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t R, uint16_t Y_R);


//void RA8876_BTE_Memory_Copy(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_Memory_Copy_Chroma_key(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_MCU_Write_MCU_8bit(uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H, const uint8_t *data);


//void RA8876_BTE_MCU_Write_MCU_16bit(uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H, const uint16_t *data);


//void RA8876_BTE_MCU_Write_Chroma_key_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H, const uint8_t *data);


//void RA8876_BTE_MCU_Write_Chroma_key_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H, const uint16_t *data);


//void RA8876_BTE_Memory_Copy_ColorExpansion(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color);


//void RA8876_BTE_Memory_Copy_ColorExpansion_Chroma_key(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color);


//void RA8876_BTE_MCU_Write_ColorExpansion_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color, const uint8_t *data);


//void RA8876_BTE_MCU_Write_ColorExpansion_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color, const uint16_t *data);


//void RA8876_BTE_MCU_Write_ColorExpansion_Chroma_key_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, const uint8_t *data);


//void RA8876_BTE_MCU_Write_ColorExpansion_Chroma_key_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, const uint16_t *data);


//void RA8876_BTE_Pattern_Fill(uint8_t P_8x8_or_16x16, uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_Pattern_Fill_With_Chroma_key(uint8_t P_8x8_or_16x16, uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint32_t Background_color, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_Solid_Fill(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Foreground_color, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_Alpha_Blending(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint8_t alpha);


//void RA8876_DMA_24bit(uint8_t SCS, uint8_t Clk, uint16_t X1, uint16_t Y1, uint16_t X_W, uint16_t Y_H, uint16_t P_W, uint32_t Addr);


//void RA8876_DMA_32bit(uint8_t SCS, uint8_t Clk, uint16_t X1, uint16_t Y1, uint16_t X_W, uint16_t Y_H, uint16_t P_W, uint32_t Addr);


//void RA8876_switch_24bits_to_32bits(uint8_t SCS);


//void RA8876_PWM0(uint8_t on_off, uint8_t Clock_Divided, uint8_t Prescalar, uint16_t Count_Buffer, uint16_t Compare_Buffer);


//void RA8876_PWM1(uint8_t on_off, uint8_t Clock_Divided, uint8_t Prescalar, uint16_t Count_Buffer, uint16_t Compare_Buffer);


//void RA8876_putPixel(uint16_t x, uint16_t y, uint32_t color);


//void RA8876_lcdPutChar8x12(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);


//void RA8876_lcdPutString8x12(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);
static int lua_RA8876_lcdPutString8x12(lua_State *L)
{
	uint16_t x = lua_tointeger(L, 1);
	uint16_t y = lua_tointeger(L, 2);
	uint32_t fgcolor = lua_tointeger(L, 3);
	uint32_t bgcolor = lua_tointeger(L, 4);
	uint8_t bg_transparent = lua_tointeger(L, 5);
	const char *ptr = lua_tostring(L, 6);
	RA8876_lcdPutString8x12(x, y, fgcolor, bgcolor, bg_transparent, (char*)ptr);
	return 0;
}

//void RA8876_lcdPutChar16x24(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);


//void RA8876_lcdPutString16x24(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);
static int lua_RA8876_lcdPutString16x24(lua_State *L)
{
	uint16_t x = lua_tointeger(L, 1);
	uint16_t y = lua_tointeger(L, 2);
	uint32_t fgcolor = lua_tointeger(L, 3);
	uint32_t bgcolor = lua_tointeger(L, 4);
	uint8_t bg_transparent = lua_tointeger(L, 5);
	const char *ptr = lua_tostring(L, 6);
	RA8876_lcdPutString16x24(x, y, fgcolor, bgcolor, bg_transparent, (char*)ptr);
	return 0;
}

//void RA8876_lcdPutChar32x48(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);


//void RA8876_lcdPutString32x48(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);
static int lua_RA8876_lcdPutString32x48(lua_State *L)
{
	uint16_t x = lua_tointeger(L, 1);
	uint16_t y = lua_tointeger(L, 2);
	uint32_t fgcolor = lua_tointeger(L, 3);
	uint32_t bgcolor = lua_tointeger(L, 4);
	uint8_t bg_transparent = lua_tointeger(L, 5);
	const char *ptr = lua_tostring(L, 6);
	RA8876_lcdPutString32x48(x, y, fgcolor, bgcolor, bg_transparent, (char*)ptr);
	return 0;
}

static int lua_RA8876_Power_Mode(lua_State *L) {
	uint16_t mode = lua_tointeger(L, 1);

	switch (mode)
	{
	case 0x00:
		Power_Normal_Mode();
	break;

	case 0x01:
		Power_Saving_Standby_Mode();
	break;

	case 0x02:
		Power_Saving_Suspend_Mode();
	break;

	case 0x03:
		Power_Saving_Sleep_Mode();
	break;
	
	default:
		break;
	}
	return 0;
}

static int lua_RA8876_Sync_Mode(lua_State *L) {
	uint16_t mode = lua_tointeger(L, 1);

	switch (mode)
	{
	case 0:
		Select_LCD_Sync_Mode();
		break;

	case 1:
		Select_LCD_DE_Mode();
		break;
	
	default:
		break;
	}

	return 0;
}

static int lua_RA8876_Reset(lua_State *L) {
	uint16_t pin_levle = lua_tointeger(L, 1);

	if(!pin_levle) {
		HAL_GPIO_WritePin(RA8876_RST_GPIO_Port, RA8876_RST_Pin, GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(RA8876_RST_GPIO_Port, RA8876_RST_Pin, GPIO_PIN_SET);
	}

	return 0;
}

const struct luaL_Reg FunctionOfRA8876[] = {


//void RA8876_MPU8_8bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);


//void RA8876_MPU8_16bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);


//void RA8876_MPU8_24bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);


//void RA8876_MPU16_16bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);


//void RA8876_MPU16_24bpp_Mode1_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);


//void RA8876_MPU16_24bpp_Mode2_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);


//void RA8876_PIP(uint8_t On_Off, uint8_t Select_PIP, uint32_t PAddr, uint16_t XP, uint16_t YP, uint32_t ImageWidth, uint16_t X_Dis, uint16_t Y_Dis, uint16_t X_W, uint16_t Y_H);


//void RA8876_Print_Internal_Font_Hexvariable(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, unsigned int tmp2);


//void RA8876_Print_Internal_Font_Decimalvariable(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, unsigned int tmp2);


//void RA8876_Print_Internal_Font_String(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char tmp2[]);


//void RA8876_Print_BIG5String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char *tmp2);


//void RA8876_Print_GB2312String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char *tmp2);


//void RA8876_Print_GB12345String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char tmp2[]);


//void RA8876_Print_UnicodeString(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, uint16_t *tmp2);


//void RA8876_Select_Font_Height_WxN_HxN_ChromaKey_Alignment(uint8_t Font_Height, uint8_t XxN, uint8_t YxN, uint8_t ChromaKey, uint8_t Alignment);


//void RA8876_Show_String(char *str);


//void RA8876_Draw_Line(uint32_t LineColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);


//void RA8876_Draw_Triangle(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X3, uint16_t Y3);


//void RA8876_Draw_Triangle_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X3, uint16_t Y3);


//void RA8876_Draw_Circle(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t R);


//void RA8876_Draw_Circle_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t R);


//void RA8876_Draw_Ellipse(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Ellipse_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Left_Up_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Left_Up_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Right_Down_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Right_Down_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Right_Up_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Right_Up_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Left_Down_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Left_Down_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);


//void RA8876_Draw_Square(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);
	{"RA8876_Draw_Square", lua_RA8876_Draw_Square},

//void RA8876_Draw_Square_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);
	{"RA8876_Draw_Square_Fill", lua_RA8876_Draw_Square_Fill},

//void RA8876_Draw_Circle_Square(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t R, uint16_t Y_R);


//void RA8876_Draw_Circle_Square_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t R, uint16_t Y_R);


//void RA8876_BTE_Memory_Copy(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_Memory_Copy_Chroma_key(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_MCU_Write_MCU_8bit(uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H, const uint8_t *data);


//void RA8876_BTE_MCU_Write_MCU_16bit(uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H, const uint16_t *data);


//void RA8876_BTE_MCU_Write_Chroma_key_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H, const uint8_t *data);


//void RA8876_BTE_MCU_Write_Chroma_key_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H, const uint16_t *data);


//void RA8876_BTE_Memory_Copy_ColorExpansion(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color);


//void RA8876_BTE_Memory_Copy_ColorExpansion_Chroma_key(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color);


//void RA8876_BTE_MCU_Write_ColorExpansion_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color, const uint8_t *data);


//void RA8876_BTE_MCU_Write_ColorExpansion_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color, const uint16_t *data);


//void RA8876_BTE_MCU_Write_ColorExpansion_Chroma_key_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, const uint8_t *data);


//void RA8876_BTE_MCU_Write_ColorExpansion_Chroma_key_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, const uint16_t *data);


//void RA8876_BTE_Pattern_Fill(uint8_t P_8x8_or_16x16, uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_Pattern_Fill_With_Chroma_key(uint8_t P_8x8_or_16x16, uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint32_t Background_color, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_Solid_Fill(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Foreground_color, uint16_t X_W, uint16_t Y_H);


//void RA8876_BTE_Alpha_Blending(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint8_t alpha);


//void RA8876_DMA_24bit(uint8_t SCS, uint8_t Clk, uint16_t X1, uint16_t Y1, uint16_t X_W, uint16_t Y_H, uint16_t P_W, uint32_t Addr);


//void RA8876_DMA_32bit(uint8_t SCS, uint8_t Clk, uint16_t X1, uint16_t Y1, uint16_t X_W, uint16_t Y_H, uint16_t P_W, uint32_t Addr);


//void RA8876_switch_24bits_to_32bits(uint8_t SCS);


//void RA8876_PWM0(uint8_t on_off, uint8_t Clock_Divided, uint8_t Prescalar, uint16_t Count_Buffer, uint16_t Compare_Buffer);


//void RA8876_PWM1(uint8_t on_off, uint8_t Clock_Divided, uint8_t Prescalar, uint16_t Count_Buffer, uint16_t Compare_Buffer);


//void RA8876_putPixel(uint16_t x, uint16_t y, uint32_t color);


//void RA8876_lcdPutChar8x12(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);


//void RA8876_lcdPutString8x12(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);
	{"RA8876_lcdPutString8x12", lua_RA8876_lcdPutString8x12},

//void RA8876_lcdPutChar16x24(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);


//void RA8876_lcdPutString16x24(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);
	{"RA8876_lcdPutString16x24", lua_RA8876_lcdPutString16x24},

//void RA8876_lcdPutChar32x48(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);


//void RA8876_lcdPutString32x48(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);
	{"RA8876_lcdPutString32x48", lua_RA8876_lcdPutString32x48},

	{"RA8876_Power_Mode", lua_RA8876_Power_Mode},

	{"RA8876_Sync_Mode", lua_RA8876_Sync_Mode},

	{"RA8876_Reset", lua_RA8876_Reset},

	{NULL, NULL},
};