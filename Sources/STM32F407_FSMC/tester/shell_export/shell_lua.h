#ifndef __LUA_SHELL_H__
#define __LUA_SHELL_H__

#include "lua.h"

extern int lua_shell (int argc, char **argv);

#endif //__LUA_SHELL_H__