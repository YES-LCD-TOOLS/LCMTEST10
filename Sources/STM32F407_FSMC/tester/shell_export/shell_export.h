#ifndef __SHELL_H__
#define __SHELL_H__

#include "ntshell.h"
#include "ntlibc.h"
#include "ntopt.h"

extern ntshell_t ntshell;
extern int func_read(char *buf, int cnt, void *extobj);
extern int func_write(const char *buf, int cnt, void *extobj);
extern int func_callback(const char *text, void *extobj);
int uart_puts(const char *s);


#endif //__SHELL_H__