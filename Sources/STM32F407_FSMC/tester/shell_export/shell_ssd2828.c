#include "ntlibc.h"
#include "shell_ssd2828.h"
#include "main.h"
#include "util.h"
#include "ssd2828.h"
#include "tester.h"

extern struct Tester test10;

int ntshell_ssd2828_ModeLP(int argc, char **argv)
{
	printf("[SSD2828] Enter LP Mode\r\n");
	return 0;
}

int ntshell_ssd2828_ModeHS(int argc, char **argv)
{
	printf("[SSD2828] Enter LP Mode\r\n");
	return 0;
}

int ntshell_ssd2828_ModeVideo(int argc, char **argv)
{
	printf("[SSD2828] Enter LP Mode\r\n");
	return 0;
}

int ntshell_ssd2828(int argc, char **argv)
{
	switch (argc) {
	case 1:
		// print usage
		printf("SSD2828 usage: \r\n");
		return 0;
		break;
	case 2:
		break;
	case 3:
		if ( (ntlibc_strcmp(argv[1], "-m") == 0) || (ntlibc_strcmp(argv[1], "--mode") == 0) ) {
			if ( ntlibc_strcmp(argv[2], "LP") == 0) {
				ssd2828_LP(&(test10.ssd2828));
				printf("SSD2828 enter LP mode\r\n");
			} else if (ntlibc_strcmp(argv[2], "HS") == 0 ){
				ssd2828_HS(&test10.ssd2828);
				printf("SSD2828 enter HS mode\r\n");
			} else if (ntlibc_strcmp(argv[2], "Video") == 0 ) {
				ssd2828_Video(&test10.ssd2828);
				printf("SSD2828 enter Video mode\r\n");
			} else {
				printf("-m --mode = [LP, HS, Video]\r\n");
			}
		}
		return 0;
		break;

	default:
		printf("Unknown sub command found\r\n");
		return -1;
		break;
	}

	return 0;
}
