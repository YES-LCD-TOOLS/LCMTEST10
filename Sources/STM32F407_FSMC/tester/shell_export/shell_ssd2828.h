#ifndef __SHELL_SSD2828_H__
#define __SHELL_SSD2828_H__

extern int ntshell_ssd2828(int argc, char **argv);
extern int ntshell_ssd2828_ModeLP(int argc, char **argv);
extern int ntshell_ssd2828_ModeHS(int argc, char **argv);
extern int ntshell_ssd2828_ModeVideo(int argc, char **argv);


#endif //__SHELL_SSD2828_H__