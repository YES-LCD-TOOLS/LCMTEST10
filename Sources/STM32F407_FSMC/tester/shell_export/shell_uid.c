#include "shell_uid.h"
#include "main.h"
#include "util.h"

int ntshell_getUID(int argc, char **argv)
{
	uint32_t uid[3];
	getUid(uid);
	printf("%08lx %08lx %08lx \r\n", uid[0], uid[1], uid[2]);
	return 0;
}