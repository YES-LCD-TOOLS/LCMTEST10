#include "shell_t10.h"
#include "ntshell.h"
#include "ntlibc.h"
#include "stdio.h"
#include "shell_export.h"
#include "tester.h"
#include "util.h"
#include "RA8876.h"

int ntshell_test10(int argc, char **argv)
{
	switch (argc) {
	case 1:
		// print usage
		printf("lcmtest10 usage: \r\n");
		return 0;
		break;
	case 2:
		if (ntlibc_strcmp(argv[1], "-s") == 0) {
			uart_puts("pause\r\n");
			return 0;
		} else if (ntlibc_strcmp(argv[1], "-n") == 0) {
			uart_puts("next\r\n");
			return 0;
		} else if (ntlibc_strcmp(argv[1], "-p") == 0) {
			uart_puts("prev\r\n");
			return 0;
		} else if (ntlibc_strcmp(argv[1], "-a") == 0) {
			uart_puts("auto\r\n");
			return 0;
		} else {
			uart_puts("Unknown sub command found\r\n");
		}
	case 3:
		if (ntlibc_strcmp(argv[1], "-f") == 0) {
			RA8876_GraphicDrawingMode();
			fillColor(htoi(argv[2]));
			printf("fill color 0x%06x\r\n", htoi(argv[2]));
			return 0;
		}
		return 0;
		break;

	default:
		printf("Unknown sub command found\r\n");
		return -1;
		break;
	}
}