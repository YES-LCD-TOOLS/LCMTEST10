#include "reg_ssd2828.h"
#include "main.h"

int reg_ssd2828_io_deinitial(void)
{
	printf("TODO");
	return -1;
}

int reg_ssd2828_io_initial(void)
{
	/**
	 * Pin map
	 * 				SSD2828
	 * XXW			Usage		MCU		TEST9
	 * 4 DOT/DC		MISO		34		io2
	 * 5 CS			CS			16		io3
	 * 6 RST		RST			64		io16
	 * 7 SCL		SCK			18		io5
	 * 8 SDA		MOSI		33		io4
	 *
	 *
	 */
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	GPIO_InitStruct.Pin = UIO_2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(UIO_2_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = UIO_3_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(UIO_3_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = UIO_4_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(UIO_4_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = UIO_5_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(UIO_5_GPIO_Port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = UIO_16_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(UIO_16_GPIO_Port, &GPIO_InitStruct);

	return 0;
}

void reg_ssd2828_set_csx(int val)
{
	HAL_GPIO_WritePin(UIO_3_GPIO_Port, UIO_3_Pin, val? GPIO_PIN_SET: GPIO_PIN_RESET);
}

void reg_ssd2828_set_sck(int val)
{
	HAL_GPIO_WritePin(UIO_5_GPIO_Port, UIO_5_Pin, val? GPIO_PIN_SET: GPIO_PIN_RESET);
}

void reg_ssd2828_set_sdi(int val)
{
	HAL_GPIO_WritePin(UIO_4_GPIO_Port, UIO_4_Pin, val? GPIO_PIN_SET: GPIO_PIN_RESET);
}

void reg_ssd2828_set_rst(int val)
{
	HAL_GPIO_WritePin(UIO_16_GPIO_Port, UIO_16_Pin, val? GPIO_PIN_SET: GPIO_PIN_RESET);
}

int reg_ssd2828_get_sdo(void)
{
	return (int)HAL_GPIO_ReadPin(UIO_2_GPIO_Port, UIO_2_Pin);
}
