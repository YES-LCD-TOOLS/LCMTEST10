#ifndef __REG_SSD2828_H__
#define __REG_SSD2828_H__

#define SSD2828_OSC 24000

int reg_ssd2828_io_initial(void);
int reg_ssd2828_io_deinitial(void);

void reg_ssd2828_set_csx(int val);
void reg_ssd2828_set_sck(int val);
void reg_ssd2828_set_sdi(int val);
void reg_ssd2828_set_rst(int val);
int reg_ssd2828_get_sdo(void);

#endif //__REG_SSD2828_H__
