#ifndef __LUA_SSD2828_H__
#define __LUA_SSD2828_H__

#include "main.h"

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

extern struct luaL_Reg functionExport_ssd2828[];

#endif //__LUA_SSD2828_H__