/**
 * @file tester.h
 * @author zhaitao (zhaitao.as@outlook.com)
 * @brief
 * @version 0.1
 * @date 2023-05-17
 *
 * @copyright zhaitao.as@outlook.com (c) 2023
 *
 */
#ifndef __TESTER_H__
#define __TESTER_H__

#include "lcd.h"
#include "main.h"
#include "ssd2828.h"

#define VERSION_MAJOR 1
#define VERSION_MINOR 12
#define VERSION_Fix 0

typedef enum {
	MODE_ONCE = 0,
	MODE_INFINITE,
	MODE_MANNUL,
	MODE_EXTERNAL
} OPERATING_MODE;

typedef enum {
	folder_struct_0 = 0,
	folder_struct_1,
} folder_struct;

struct Version {
	uint32_t major;
	uint32_t miner;
	uint32_t fix;
};

struct Tester {
	uint8_t *gram_addr;
	uint32_t gram_size;

	struct Version version;
	int debug;
	int auto_power_off;
	uint32_t img_interval_ms;
	uint8_t img_count;
	uint8_t img_index;
	char img_full_path[64];
	uint8_t flag_pause;

	MIPI_DISPLAY_TYPEDEF mipi_type;

	struct ssd2828_config ssd2828;
	struct LCD_Timing timing;
	struct LCD_Timing timing_real;
	uint16_t fps;
	uint16_t fps_real;
	uint8_t dsi_lane_number;
	uint8_t dsi_mode;
};

int testModeStart(void);
int usbModeStart(void);
void fillColor(uint32_t color);
uint8_t drawFile(char *fn);

/**
 * @brief Set the lvds color mode object
 * 
 * @param v : 0=VESA, 1=JEIDA
 */
void set_lvds_color_mode(uint16_t v);

#endif //__TESTER_H__
