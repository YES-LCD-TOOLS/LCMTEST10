#ifndef __LUA_FUNC_LIB_H__
#define __LUA_FUNC_LIB_H__

#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"

#define FunctionOfLcmtest10_Counts 60
extern struct luaL_Reg FunctionOfLcmtest10[FunctionOfLcmtest10_Counts];

/**
 * @brief run a lua file
 *
 * @param L lua vm
 * @param filename lua file
 */
void runLua(lua_State *L, const char *filename);

#endif //__LUA_FUNC_LIB_H__
