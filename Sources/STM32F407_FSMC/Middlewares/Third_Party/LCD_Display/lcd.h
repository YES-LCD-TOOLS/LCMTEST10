/*
 * @Author: zhaitao
 * @Date: 2020-06-02 13:07:55
 * @LastEditTime: 2020-06-10 14:12:12
 * @LastEditors: zhai tao
 * @Description: In User Settings Edit
 * @FilePath: \STM32F407_FSMC\lcd\lcd.h
 */
#ifndef __LCD_H__
#define __LCD_H__

#include "main.h"
#include "stm32f4xx_hal.h"

typedef enum {
	NOT_SET = 0,
	MIPI_DBI_A, // M6800
	MIPI_DBI_B, // I8080
	MIPI_DBI_C_1, // SPI
	MIPI_DBI_C_2, // SPI
	MIPI_DBI_C_3, // SPI
	MIPI_DPI, // RGB
	MIPI_DSI, // Serial
} MIPI_DISPLAY_TYPEDEF;

typedef enum {
	DSI_COMMAND_MODE = -1, // Command Mode
	DSI_VIDEO_SP, // Non burst mode with sync pulses
	DSI_VIDEO_SE, // Non burst mode with sync events
	DSI_VIDEO_BURST, // Burst mode
} DSI_MODE_TYPEDEF;

struct LCD_Timing {
	uint16_t ha; // h size
	uint16_t hsw; // h pause width
	uint16_t hbp; // h back porch
	uint16_t hfp; // h front porch
	uint16_t va; // v size
	uint16_t vsw; // v pause width
	uint16_t vbp; // v back porch
	uint16_t vfp; // v front porch
	uint8_t hsp : 1; // hsync polarity
	uint8_t vsp : 1; // vsync polarity
	uint8_t dep : 1; // de polarity
	uint8_t clkEdge : 1; // data latch clock edge
};

extern uint32_t calcPixelClock(struct LCD_Timing *lcd, uint8_t fps);
extern uint32_t calcFPS(struct LCD_Timing *lcd, uint32_t pclk);
extern void print_dpi_timing(struct LCD_Timing *lcd);
void convert_to_jeida_format(uint8_t *lineData, uint32_t lineDataSize);

#endif //__LCD_H__