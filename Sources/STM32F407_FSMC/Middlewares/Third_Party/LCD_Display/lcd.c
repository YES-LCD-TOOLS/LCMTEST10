/*
 * @Author: zhai tao
 * @Date: 2020-06-02 13:07:55
 * @LastEditors: zhai tao
 * @LastEditTime: 2020-06-10 14:17:40
 * @FilePath: \STM32F407_FSMC\lcd\lcd.c
 */
#include "lcd.h"

/**
 * @brief Get Pixel Clock from LCD timing and given FPS
 *
 * @param lcd
 * @param fps
 * @return uint32_t
 */
uint32_t calcPixelClock(struct LCD_Timing *lcd, uint8_t fps)
{
	return (lcd->ha + lcd->hsw + lcd->hbp + lcd->hfp) *
	       (lcd->va + lcd->vsw + lcd->vbp + lcd->vfp) * (uint32_t)fps;
}

/**
 * @brief Get FPS from LCD timing and given pixel clock
 *
 * @param lcd
 * @param pclk
 * @return uint32_t
 */
uint32_t calcFPS(struct LCD_Timing *lcd, uint32_t pclk)
{
	return pclk / (lcd->ha + lcd->hsw + lcd->hbp + lcd->hfp) /
	       (lcd->va + lcd->vsw + lcd->vbp + lcd->vfp);
}

/**
 * @brief Print LCD Timing Infomation
 *
 * @param lcd
 */
void print_dpi_timing(struct LCD_Timing *lcd)
{
	printf("[DPI] Timming Settings: \r\n \
		HA=%d, HSW=%d, HBP=%d, HFP=%d, \r\n \
		VA=%d, VSW=%d, VBP=%d, VFP=%d, \r\n \
		HSP=%d, VSP=%d, DEP=%d, CLKP=%d \r\n",
	       lcd->ha, lcd->hsw, lcd->hbp, lcd->hfp, lcd->va, lcd->vsw,
	       lcd->vbp, lcd->vfp, lcd->hsp, lcd->vsp, lcd->dep, lcd->clkEdge);
}

void convert_to_jeida_format(uint8_t *lineData, uint32_t lineDataSize)
{
	uint8_t r, g, b;
	for (uint32_t i = 0; i < lineDataSize; i += 3) {
		r = lineData[i];
		g = lineData[i + 1];
		b = lineData[i + 2];

		lineData[i] = ((r >> 2) & 0x3F) | (r << 6); // R2-R7 + R0-R1
		lineData[i + 1] = ((g >> 2) & 0x3F) | (g << 6); // G2-G7 + G0-G1
		lineData[i + 2] = ((b >> 2) & 0x3F) | (b << 6); // B2-B7 + B0-B1
	}
}