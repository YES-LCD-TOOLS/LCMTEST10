/**
 * @file video_sync_polarity.h
 * @author Zhai Tao (zhaitao.as@outlook.com)
 * @brief 
 * @version 0.1
 * @date 2022-03-23
 * 
 * @copyright zhaitao.as@outlook.com (c) 2022
 * 
 */

#ifndef __VIDEO_SYNC_POLARITY_H__
#define __VIDEO_SYNC_POLARITY_H__

struct lcd_sync_polarity {
	int hsp : 1;     // hsync polarity
	int vsp : 1;     // vsync polarity
	int dep : 1;     // de polarity
	int clkEdge : 1; // data latch clock edge
};

#endif //__VIDEO_SYNC_POLARITY_H__
