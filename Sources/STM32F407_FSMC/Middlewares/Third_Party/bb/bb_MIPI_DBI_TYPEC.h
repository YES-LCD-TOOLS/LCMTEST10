#ifndef __BB_MIPI_DBI_TYPEC_H__
#define __BB_MIPI_DBI_TYPEC_H__

#include "bb.h"
#include <stdint.h>

struct bb_mipi_dbi_typec_host_initial_typedef
{
	void (*_delay_ns)(unsigned long);
	BB_U32 speed;
	BB_U8 byte_len;

	/*struct bb_pin *sck;
	struct bb_pin *mosi;
	struct bb_pin *miso;
	struct bb_pin *cs;
	struct bb_pin *dc;*/
	void (*write_com)(BB_U8 data, BB_BIT do_start, BB_BIT do_stop);
	void (*write_dat)(BB_U8 data, BB_BIT do_start, BB_BIT do_stop);
	void (*read_dat)(BB_U8 data, BB_BIT do_start, BB_BIT do_stop);
};

struct bb_mipi_dbi_typec_host_typedef {
	BB_U32 half_clk; /* micro second */
	struct bb_mipi_dbi_typec_host_initial_typedef *init_obj;
};


#endif //__BB_MIPI_DBI_TYPEC_H__
