/**
 * @file bb_spi_master.h
 * @author Zhai Tao (zhaitao.as@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-03-03
 *
 * @copyright zhaitao.as@outlook.com (c) 2022
 *
 */

#ifndef __BB_SPI_MASTER_H__
#define __BB_SPI_MASTER_H__

#include "bb.h"
#include "bb_pin.h"

#define BANG_SPI_BIT_ORDER_MSBFIRST 0
#define BANG_SPI_BIT_ORDER_LSBFIRST 1

/**
 * @brief
 *
 */
struct bb_spi_master_initial_typedef {
	void (*_delay_ns)(unsigned long);
	BB_U32 speed;
	BB_U8 byte_len;
	BB_BIT cpol:1; /* 0:inactive low, 1:inactive high */
	BB_BIT cpha:1; /* 0:data latch at heading edge 1:data latch at trailing edge */
	BB_BIT bit_order:1; /* 0:MSB first, 1:LSB first */
	BB_U8 dir:2; /* 0:write only, 1:read only, 2:full duplex */

	struct bb_pin_config_typedef *config;
	struct bb_pin_typedef *cs;
	struct bb_pin_typedef *sck;
	struct bb_pin_typedef *mosi;
	struct bb_pin_typedef *miso;
};

/**
 * @brief
 * 
 * @param BB_U32 half_clk
 * @param bb_spi_master_initial_typedef *init_obj
 */
struct bb_spi_master {
	BB_U32 half_clk; /* micro second */
	struct bb_spi_master_initial_typedef *init_obj;
};

/**
 * @brief
 *
 * @param dev
 * @param init_obj
 */
void bb_spi_master_initial(struct bb_spi_master *spi, struct bb_spi_master_initial_typedef *init_obj);

/**
 * @brief
 *
 * @param spi
 * @param dev_addr
 * @param buffer
 * @param len
 */
BB_BOOL bb_spi_master_send(struct bb_spi_master *spi, struct bb_byte data[], unsigned int size);

/**
 * @brief 
 * 
 * @param spi 
 * @param data 
 * @param size 
 * @return BB_BOOL 
 */
BB_BOOL bb_spi_master_recive(struct bb_spi_master *spi, struct bb_byte data[], unsigned int size);


void bb_spi_send_n(struct bb_spi_master *spi, struct bb_byte *outbuf, unsigned int len, BB_BIT en_start, BB_BIT en_end);
void bb_spi_recive_n(struct bb_spi_master *spi, struct bb_byte *inbuf, unsigned int len, BB_BIT en_start, BB_BIT en_end);
struct bb_byte* bb_spi_transfer_n(struct bb_spi_master *spi, struct bb_byte *outbuf, struct bb_byte *inbuf, unsigned int len);
#endif //__BB_SPI_MASTER_H__
