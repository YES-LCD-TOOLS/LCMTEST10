#ifndef __DEBUG_H__
#define __DEBUG_H__

enum BB_Debug_Level {
	BB_DBG_NONE,
	BB_DBG_ERROR,
	BB_DBG_WARRNING,
	BB_DBG_INFO,
};

#endif //__DEBUG_H__
