#ifndef __BB_PIN_H__
#define __BB_PIN_H__

#include "bb.h"

enum BB_PIN_MODE {
	BB_PIN_MODE_IN,
	BB_PIN_MODE_OUT,
	BB_PIN_MODE_ALTER,
	BB_PIN_MODE_ANALOG,
};

enum BB_PIN_OUT_TYPE {
	BB_PIN_OUT_TYPE_PP,
	BB_PIN_OUT_TYPE_OD,
};

enum BB_PIN_SPEED {
	BB_PIN_SPEED_LOW,
	BB_PIN_SPEED_MEDIUM,
	BB_PIN_SPEED_HIGH,
	BB_PIN_SPEED_VERY_HIGH,
};

enum BB_PIN_PULL {
	BB_PIN_PULL_NO,
	BB_PIN_PULL_UP,
	BB_PIN_PULL_DOWN,
};

/**
 * @brief Pin config function callback pointers
 *
 * @param set void (*set)(void *port, BB_U32 pin, BB_U32 setting);
 * @param get BB_U8 (*get)(void *port, BB_U32 pin);
 * @param set_mode void (*set_mode)(void *port, BB_U32 pin, enum BB_PIN_MODE setting);
 * @param set_out_type void (*set_out_type)(void *port, BB_U32 pin, enum BB_PIN_OUT_TYPE setting);
 * @param set_pull_type void (*set_pull_type)(void *port, BB_U32 pin, enum BB_PIN_PULL setting);
 * @param set_speed void (*set_speed)(void *port, BB_U32 pin, enum BB_PIN_SPEED setting);
 * @param set_af void (*set_af)(void *port, BB_U32 pin, BB_U32 setting);
 */
struct bb_pin_config_typedef {
	void (*set)(void *port, BB_U32 pin, BB_U32 setting);
	BB_U8 (*get)
	(void *port, BB_U32 pin);
	void (*set_mode)(void *port, BB_U32 pin, enum BB_PIN_MODE setting);
	void (*set_out_type)(void *port, BB_U32 pin, enum BB_PIN_OUT_TYPE setting);
	void (*set_pull_type)(void *port, BB_U32 pin, enum BB_PIN_PULL setting);
	void (*set_speed)(void *port, BB_U32 pin, enum BB_PIN_SPEED setting);
	void (*set_af)(void *port, BB_U32 pin, BB_U32 setting);
};

/**
 * @brief
 *
 * @param name pin name char*8
 * @param is_inuse BB_BIT
 * @param port void*
 * @param pin BB_U32
 */
struct bb_pin_typedef {
	char name[8];
	BB_BIT is_inuse;
	void *port;
	BB_U32 pin;
};

BB_BOOL bb_pin_require(char *name, struct bb_pin_typedef *pin);

BB_BOOL bb_pin_release(struct bb_pin_typedef *pin);

#define bb_pin_set(d, p, x) d->init_obj->config->set(d->init_obj->p->port, d->init_obj->p->pin, x)
#define bb_pin_get(d, p) d->init_obj->config->get(d->init_obj->p->port, d->init_obj->p->pin)

int gpio_set_value(int pin, int value);
int gpio_get_value(int pin);
int gpio_request(int pin, const char *pname);
int gpio_direction_output(int pin, int value);
int gpio_direction_input(int pin);
int gpio_free(int pin);

#endif //__BB_PIN_H__
