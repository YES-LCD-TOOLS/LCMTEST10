/**
 * @file bb_spi_master.c
 * @author Zhai Tao (zhaitao.as@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-03-03
 *
 * @copyright zhaitao.as@outlook.com (c) 2022
 *
 */

#include "bb_spi_master.h"
#include "bb_pin.h"
#include <memory.h>
#include <stdio.h>

#define HIGH 1
#define LOW 0

#define true 1
#define false 0

// pin set
#define _cs(x) spi->init_obj->config->set(spi->init_obj->cs->port, spi->init_obj->cs->pin, x)
#define _sck(x) spi->init_obj->config->set(spi->init_obj->sck->port, spi->init_obj->sck->pin, x)
#define _mosi(x) spi->init_obj->config->set(spi->init_obj->mosi->port, spi->init_obj->mosi->pin, x)
#define _miso(x) spi->init_obj->config->get(spi->init_obj->miso->port, spi->init_obj->miso->pin)

/*
 ***** Private Fuctions *****
 */

static inline void _SPI_start_cond(struct bb_spi_master *spi)
{
	bb_pin_set(spi, cs, 0);
}

static inline void _SPI_stop_cond(struct bb_spi_master *spi)
{
	bb_pin_set(spi, cs, 1);
}

static inline void SPI_write_bit(struct bb_spi_master *spi, BB_U8 b)
{
	// if (b > 0)
	// SPI_SET_SDA;
	// else
	// SPI_CLEAR_SDA;

	// SPI_DELAY;
	// SPI_SET_SCL;
	// SPI_DELAY;
	// SPI_CLEAR_SCL;
}

static inline BB_U8 SPI_read_SDA(struct bb_spi_master *spi)
{
	return 0;
}

static inline BB_U8 SPI_read_bit(struct bb_spi_master *spi)
{
	BB_U8 b;

	return b;
}

static inline _Bool SPI_write_byte(struct bb_spi_master *spi, BB_U8 B, _Bool start, _Bool stop)
{
	return 0;
}

// Reading a byte with SPI:
static inline BB_U8 SPI_read_byte(struct bb_spi_master *spi, BB_BOOL ack, BB_BOOL stop)
{
	return 0;
}

// Sending a byte with SPI:
static inline _Bool SPI_send_byte(struct bb_spi_master *spi, BB_U8 address, BB_U8 data)
{
	return 0;
}

// Receiving a byte with a SPI:
static inline BB_U8 SPI_receive_byte(struct bb_spi_master *spi, BB_U8 address)
{
	return 0;
}

// Sending a byte of data with SPI:
static inline _Bool SPI_send_byte_data(struct bb_spi_master *spi, BB_U8 address, BB_U8 reg, BB_U8 data)
{

	return false;
}

// Receiving a byte of data with SPI:
static inline BB_U8 SPI_receive_byte_data(struct bb_spi_master *spi, BB_U8 address, BB_U8 reg)
{

	return 0; // return zero if NACKed
}

/*
 ***** Public Fuctions *****
 */

void bb_spi_master_initial(struct bb_spi_master *spi, struct bb_spi_master_initial_typedef *init_obj)
{
	spi->init_obj = init_obj;

	double cycle = 1000000000.0 / init_obj->speed;
	spi->half_clk = cycle / 2;
}

BB_BOOL bb_spi_master_send(struct bb_spi_master *spi, struct bb_byte data[], unsigned int size)
{

	return false;
}

struct bb_byte *bb_spi_transfer_n(struct bb_spi_master *spi, struct bb_byte *outbuf, struct bb_byte *inbuf, unsigned int len)
{
	// empty inbuf
	memset(inbuf, 0x00, len);

	// convert outbuf to LSBFIRST
	if (spi->init_obj->bit_order == BANG_SPI_BIT_ORDER_LSBFIRST) {
		bang_spi_reverse_bits(outbuf, len);
	}

	// get clock's current idle state
	// int clock = _cpol ? HIGH : LOW;
	int clock = spi->init_obj->cpol ? 1 : 0;

	// pull down CS
	// bcm2835_gpio_write(_cs, LOW);
	// spi->init_obj->cs->set(LOW);
	bb_pin_set(spi, cs, 0);

	for (int8_t k = 0; k < len; k++) {
		for (int8_t i = outbuf[k].byte_len; i >= 0; i--) {
			// if (_cpha) {
			if (spi->init_obj->cpha) {
				// set clock to active
				clock ^= 1;
				// spi->init_obj->sck->set(clock);
				bb_pin_set(spi, sck, clock);
			}

			// tx 1bit
			bb_pin_set(spi, mosi, (outbuf[k].B.byte >> i & 1) ? HIGH : LOW);

			// bang_spi_wait();
			spi->init_obj->_delay_ns(spi->half_clk);

			// flip the clock signal
			clock ^= 1;
			// spi->init_obj->sck->set(clock);
			bb_pin_set(spi, sck, clock);

			// bang_spi_wait();
			spi->init_obj->_delay_ns(spi->half_clk);

			// rx 1bit
			// inbuf[k].B.byte |= spi->init_obj->miso->get();
			inbuf[k].B.byte |= _miso();

			// if (!_cpha) {
			if (spi->init_obj->cpha) {
				// set clock to idle
				clock ^= 1;
				// spi->init_obj->sck->set(clock);
				bb_pin_set(spi, sck, clock);
			}
		}
	}

	// pull up CS
	// spi->init_obj->cs->set(HIGH);
	_cs(HIGH);

	/* is inbuf conversion desirable?
	// convert value to LSBFIRST
	if (_bit_order == LSBFIRST)
	{
	    bang_spi_reverse_bits(inbuf, len);
	}
	*/

	return inbuf;
}

void bb_spi_send_n(struct bb_spi_master *spi, struct bb_byte *outbuf, unsigned int len, BB_BIT en_start, BB_BIT en_end)
{
	// convert outbuf to LSBFIRST
	if (spi->init_obj->bit_order == BANG_SPI_BIT_ORDER_LSBFIRST) {
		bang_spi_reverse_bits(outbuf, len);
	}

	// get clock's current idle state
	int clock = spi->init_obj->cpol ? 1 : 0;

	// pull down CS
	if (en_start) {
		// spi->init_obj->cs->set(LOW);
		_cs(LOW);
	}

	for (int8_t k = 0; k < len; k++) {
		for (int8_t i = outbuf[k].byte_len; i >= 0; i--) {
			// if (_cpha) {
			if (spi->init_obj->cpha) {
				// set clock to active
				clock ^= 1;
				// spi->init_obj->sck->set(clock);
				bb_pin_set(spi, sck, clock);
			}

			// tx 1bit
			bb_pin_set(spi, mosi, (outbuf[k].B.byte >> i & 1) ? HIGH : LOW);

			// bang_spi_wait();
			spi->init_obj->_delay_ns(spi->half_clk);

			// flip the clock signal
			clock ^= 1;
			// spi->init_obj->sck->set(clock);
			bb_pin_set(spi, sck, clock);

			// bang_spi_wait();
			spi->init_obj->_delay_ns(spi->half_clk);

			// rx 1bit
			// inbuf[k].B.byte |= spi->init_obj->miso->get();

			// if (!_cpha) {
			if (spi->init_obj->cpha) {
				// set clock to idle
				clock ^= 1;
				// bcm2835_gpio_write(_sck, clock);
				// spi->init_obj->sck->set(clock);
				bb_pin_set(spi, sck, clock);
			}
		}
	}

	// pull up CS
	if (en_end) {
		bb_pin_set(spi, cs, HIGH);
	}

	/* is inbuf conversion desirable?
	// convert value to LSBFIRST
	if (_bit_order == LSBFIRST)
	{
	    bang_spi_reverse_bits(inbuf, len);
	}
	*/
}

void bb_spi_recive_n(struct bb_spi_master *spi, struct bb_byte *inbuf, unsigned int len, BB_BIT en_start, BB_BIT en_end)
{
	// empty inbuf
	memset(inbuf, 0x00, len);

	// get clock's current idle state
	int clock = spi->init_obj->cpol ? 1 : 0;

	// pull down CS
	if (en_start) {
		// spi->init_obj->cs->set(LOW);
		_cs(LOW);
	}

	for (int8_t k = 0; k < len; k++) {
		for (int8_t i = 7; i >= 0; i--) {
			// if (_cpha) {
			if (spi->init_obj->cpha) {
				// set clock to active
				clock ^= 1;
				// spi->init_obj->sck->set(clock);
				bb_pin_set(spi, sck, clock);
			}

			// tx 1bit
			// spi->init_obj->mosi->set((outbuf[k].B.byte >> i & 1) ? HIGH : LOW);

			// bang_spi_wait();
			spi->init_obj->_delay_ns(spi->half_clk);

			// flip the clock signal
			clock ^= 1;
			// spi->init_obj->sck->set(clock);
			bb_pin_set(spi, sck, clock);

			// bang_spi_wait();
			spi->init_obj->_delay_ns(spi->half_clk);

			// rx 1bit
			// inbuf[k].B.byte |= spi->init_obj->miso->get();

			// if (!_cpha) {
			if (spi->init_obj->cpha) {
				// set clock to idle
				clock ^= 1;
				// bcm2835_gpio_write(_sck, clock);
				// spi->init_obj->sck->set(clock);
				bb_pin_set(spi, sck, clock);
			}
		}
	}

	// pull up CS
	if (en_end) {
		// spi->init_obj->cs->set(HIGH);
		_cs(HIGH);
	}

	/* is inbuf conversion desirable?
	// convert value to LSBFIRST
	if (_bit_order == LSBFIRST)
	{
	    bang_spi_reverse_bits(inbuf, len);
	}
	*/
}
