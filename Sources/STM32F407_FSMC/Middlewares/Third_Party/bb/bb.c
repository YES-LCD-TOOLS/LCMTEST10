#include "bb.h"

void bang_spi_reverse_bits(struct bb_byte *buf, BB_U32 len)
{
	BB_U32 n = 0;
	BB_U8 byte_len = 0;
	for (BB_U8 i = 0; i < len; i++) {
		byte_len = buf[i].byte_len;
		n = buf[i].B.byte;
		n = ((n >> 1) & 0x55555555) | ((n << 1) & 0xaaaaaaaa);
		n = ((n >> 2) & 0x33333333) | ((n << 2) & 0xcccccccc);
		n = ((n >> 4) & 0x0f0f0f0f) | ((n << 4) & 0xf0f0f0f0);
		n = ((n >> 8) & 0x00ff00ff) | ((n << 8) & 0xff00ff00);
		n = ((n >> 16) & 0x0000ffff) | ((n << 16) & 0xffff0000);

		n = n >> (32 - byte_len);
		buf[i].B.byte = n;
	}
}