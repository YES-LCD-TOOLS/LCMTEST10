/**
 * @file bb.h
 * @author Zhai Tao (zhaitao.as@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-03-03
 *
 * @copyright zhaitao.as@outlook.com (c) 2022
 *
 */

#ifndef __BB_H__
#define __BB_H__

#include "bb_port.h"

#define BB_BIT unsigned char
#define BB_BOOL unsigned char
#define BB_U8 unsigned char
#define BB_U16 unsigned short
#define BB_U32 unsigned int

#define BB_LOW 0
#define BB_HIGH (~BB_LOW)

#define BB_TRUE 0
#define BB_FALSE (~BB_TRUE)

#define BB_I2C_MEM_8BIT 0
#define BB_I2C_MEM_16BIT 1

#define BB_OK 0
#define BB_ERR_GEN 1
#define BB_ERR_PIN_IN_USE 2


typedef union {
	unsigned int byte;
	struct
	{
		BB_BIT _00 : 1;
		BB_BIT _01 : 1;
		BB_BIT _02 : 1;
		BB_BIT _03 : 1;
		BB_BIT _04 : 1;
		BB_BIT _05 : 1;
		BB_BIT _06 : 1;
		BB_BIT _07 : 1;
		BB_BIT _08 : 1;
		BB_BIT _09 : 1;
		BB_BIT _10 : 1;
		BB_BIT _11 : 1;
		BB_BIT _12 : 1;
		BB_BIT _13 : 1;
		BB_BIT _14 : 1;
		BB_BIT _15 : 1;
		BB_BIT _16 : 1;
		BB_BIT _17 : 1;
		BB_BIT _18 : 1;
		BB_BIT _19 : 1;
		BB_BIT _20 : 1;
		BB_BIT _21 : 1;
		BB_BIT _22 : 1;
		BB_BIT _23 : 1;
		BB_BIT _24 : 1;
		BB_BIT _25 : 1;
		BB_BIT _26 : 1;
		BB_BIT _27 : 1;
		BB_BIT _28 : 1;
		BB_BIT _29 : 1;
		BB_BIT _30 : 1;
		BB_BIT _31 : 1;
	} bits;
} bb_4B;

struct bb_byte {
	BB_U8 byte_len;
	bb_4B B;
};

/**
 * @brief 
 * 
 * @param buf 
 * @param len 
 */
void bang_spi_reverse_bits(struct bb_byte *buf, BB_U32 len);

#endif //__BB_H__
