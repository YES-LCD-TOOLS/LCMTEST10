/**
 * @file bb_i2c_master.h
 * @author Zhai Tao (zhaitao.as@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-03-03
 *
 * @copyright zhaitao.as@outlook.com (c) 2022
 *
 */

#ifndef __BB_I2C_MASTER_H__
#define __BB_I2C_MASTER_H__

#include "bb.h"

/**
 * @brief
 *
 */
struct bb_i2c_master_initial_typedef {
	BB_U8 mem_address_len : 1; /* 0:8bits, 1:16bits */
	BB_U32 speed;		   /* Hz */
	void (*_delay_us)(long unsigned);
	void (*scl_dir)(BB_BOOL); /* low: out, high: in */
	void (*scl_set)(BB_BOOL); /* write */
	BB_BOOL (*scl_get)
	(void);			  /* read */
	void (*sda_dir)(BB_BOOL); /* low: out, high: in */
	void (*sda_set)(BB_BOOL); /* write */
	BB_BOOL (*sda_get)
	(void); /* read */
};

/**
 * @brief
 *
 */
struct bb_i2c_master {
	BB_U32 half_clk; /* micro second */
	struct bb_i2c_master_initial_typedef *init_obj;
};

/**
 * @brief
 *
 * @param dev
 * @param init_obj
 */
void bb_i2c_master_initial(struct bb_i2c_master *i2c, struct bb_i2c_master_initial_typedef *init_obj);

/**
 * @brief
 *
 * @param i2c
 * @param dev_addr
 * @param buffer
 * @param len
 */
BB_BOOL bb_i2c_master_send(struct bb_i2c_master *i2c, BB_U16 dev_addr, BB_U8 *buffer, BB_U32 len);

/**
 * @brief
 *
 * @param i2c
 * @param dev_addr
 * @param buffer
 * @param len
 */
BB_BOOL bb_i2c_master_recive(struct bb_i2c_master *i2c, BB_U16 dev_addr, BB_U8 reg[], BB_U8 reg_len, BB_U8 *buffer, BB_U32 len);

/**
 * @brief
 *
 * @param i2c
 * @param dev_addr
 * @param mem_addr
 * @param buffer
 * @param len
 */
void bb_i2c_master_mem_write(struct bb_i2c_master *i2c, BB_U16 dev_addr, BB_U16 mem_addr, BB_U8 *buffer, BB_U32 len);

/**
 * @brief
 *
 * @param i2c
 * @param dev_addr
 * @param mem_addr
 * @param buffer
 * @param len
 */
void bb_i2c_master_mem_read(struct bb_i2c_master *i2c, BB_U16 dev_addr, BB_U16 mem_addr, BB_U8 *buffer, BB_U32 len);

void bb_i2c_slave_detect(struct bb_i2c_master *i2c);
#endif //__BB_I2C_MASTER_H__
