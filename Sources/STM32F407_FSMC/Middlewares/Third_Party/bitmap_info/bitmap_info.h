#ifndef __BITMAP_INFO_H__
#define __BITMAP_INFO_H__

#include <stdint.h>


#pragma pack(push)
#pragma pack(1)
/*位图文件文件头结构体*/
typedef struct tagBITMAPFILEHEADER {
	uint16_t bFileType;
	uint32_t bFileSize;
	uint32_t bReserved1;
	//uint16_t bReserved2;
	uint32_t bPixelDataOffset;
}BITMAPFILEHEADER; //14bytes
#pragma pack(pop)

/*位图文件信息头结构体*/
typedef struct tagBITMAPINFOHEADER {
	uint32_t bHeaderSize;  // 图像信息头总大小（40bytes）
	uint32_t bImageWidth;  // 图像宽度（像素）
	uint32_t bImageHeight;  // 图像高度
	uint16_t bPlanes;  // 应该是0
	uint16_t bBitsPerPixel;  // 像素位数
	uint32_t bCompression;  // 图像压缩方法
	uint32_t bImageSize;  // 图像大小（字节）
	uint32_t bXpixelsPerMeter;  // 横向每米像素数
	uint32_t bYpixelsPerMeter;  // 纵向每米像素数
	uint32_t bTotalColors;  // 使用的颜色总数，如果像素位数大于8，则该字段没有意义
	uint32_t bImportantColors;  // 重要颜色数，一般没什么用
}BITMAPINFOHEADER; //40byte

typedef struct BITMAP_HEADER_Typedef {
	BITMAPFILEHEADER file_header;
	BITMAPINFOHEADER info_header;
}BITMAP_HEADER; // 54Bytes

typedef struct tagRGBQUAD {
	uint8_t	rgbBlue;
	uint8_t	rgbGreen;
	uint8_t	rgbRed;
	uint8_t	rgbReserved;
}RGBQUAD;

/*像素点RGB结构体*/
typedef struct tagRGB {
	uint8_t blue;
	uint8_t green;
	uint8_t red;
}RGBDATA;


#endif // !__BITMAP_INFO_H__