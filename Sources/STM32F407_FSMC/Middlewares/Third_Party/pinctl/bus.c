#include "bus.h"
#include "util.h"

/**
 * @brief eI2C
 *
 */

GPIO_TypeDef *ei2c_sda_port;
uint16_t ei2c_sda_pin;
GPIO_TypeDef *ei2c_scl_port;
uint16_t ei2c_scl_pin;
uint8_t ack;
uint8_t slaveAddr;
#define EI2C_SPEED_LOW 140
#define EI2C_SPEED_STANDARD 11
#define EI2C_SPEED_HIGH 2
volatile uint8_t ei2c_speed = EI2C_SPEED_STANDARD;

int lua_ei2c_init(lua_State *L);
void ei2c_sda(uint8_t state);
void ei2c_scl(uint8_t state);
void ei2c_sdaOut(void);
void ei2c_sdaIn(void);
uint8_t ei2c_getSda(void);
void ei2c_delayUs(uint32_t uscount);
static void ei2c_start(void);
static void ei2c_stop(void);
static void ei2c_sendByte(uint8_t c);
static uint8_t ei2c_rcvByte(void);
static void ei2c_ack(uint8_t a);
static uint8_t ei2c_memWrite(uint8_t sla, uint8_t suba, uint8_t *s, uint8_t no);
static uint8_t ei2c_memRead(uint8_t sla, uint8_t suba, uint8_t *s, uint8_t no);
uint8_t ei2c_read1Byte(uint8_t RegAddr);
int lua_ei2c_read1Byte(lua_State *L);
uint8_t ei2c_readNByte(uint8_t RegAddr, uint8_t *p_data, uint8_t N);
uint8_t ei2c_write1Byte(uint8_t RegAddr, uint8_t d);
int lua_ei2c_write1Byte(lua_State *L);
uint8_t ei2c_writeNByte(uint8_t RegAddr, uint8_t *d, uint8_t N);

/**
 * @brief set emulating i2c
 *
 * @param io_sda sda pin
 * @param io_scl scl pin
 * @param addr slave address
 * @param speed 10, 100, 400(kHz)
 * @return int
 */
int lua_ei2c_init(lua_State *L)
{
	int io_sda = lua_tointeger(L, 1);
	int io_scl = lua_tointeger(L, 2);
	int addr = lua_tointeger(L, 3);
	int speed = lua_tointeger(L, 4);

	printf("lua_ei2c_init sda %d scl %d slave addr %02x speed %d\r\n", io_sda, io_scl, addr, speed);

	/* set sda gpio */
	switch (io_sda) {
	case 2:
		ei2c_sda_port = UIO_2_GPIO_Port;
		ei2c_sda_pin = UIO_2_Pin;
		break;

	case 3:
		ei2c_sda_port = UIO_3_GPIO_Port;
		ei2c_sda_pin = UIO_3_Pin;
		break;

	case 4:
		ei2c_sda_port = UIO_4_GPIO_Port;
		ei2c_sda_pin = UIO_4_Pin;
		break;

	case 5:
		ei2c_sda_port = UIO_5_GPIO_Port;
		ei2c_sda_pin = UIO_5_Pin;
		break;

	case 14:
		ei2c_sda_port = UIO_14_GPIO_Port;
		ei2c_sda_pin = UIO_14_Pin;
		break;

	case 15:
		ei2c_sda_port = UIO_15_GPIO_Port;
		ei2c_sda_pin = UIO_15_Pin;
		break;

	case 16:
		ei2c_sda_port = UIO_16_GPIO_Port;
		ei2c_sda_pin = UIO_16_Pin;
		break;

	default:
		printf("ei2c pin setting ERROR!\r\n");
		break;
	}

	/* set scl gpio */
	switch (io_scl) {
	case 2:
		ei2c_scl_port = UIO_2_GPIO_Port;
		ei2c_scl_pin = UIO_2_Pin;
		break;

	case 3:
		ei2c_scl_port = UIO_3_GPIO_Port;
		ei2c_scl_pin = UIO_3_Pin;
		break;

	case 4:
		ei2c_scl_port = UIO_4_GPIO_Port;
		ei2c_scl_pin = UIO_4_Pin;
		break;

	case 5:
		ei2c_scl_port = UIO_5_GPIO_Port;
		ei2c_scl_pin = UIO_5_Pin;
		break;

	case 14:
		ei2c_scl_port = UIO_14_GPIO_Port;
		ei2c_scl_pin = UIO_14_Pin;
		break;

	case 15:
		ei2c_scl_port = UIO_15_GPIO_Port;
		ei2c_scl_pin = UIO_15_Pin;
		break;

	case 16:
		ei2c_scl_port = UIO_16_GPIO_Port;
		ei2c_scl_pin = UIO_16_Pin;
		break;

	default:
		printf("ei2c pin setting ERROR!\r\n");
		break;
	}

	/* gpio init */
	HAL_GPIO_WritePin(ei2c_sda_port, ei2c_sda_pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(ei2c_scl_port, ei2c_scl_pin, GPIO_PIN_SET);

	GPIO_InitTypeDef GPIO_InitStruct = {0};

	GPIO_InitStruct.Pin = ei2c_sda_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ei2c_sda_port, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = ei2c_scl_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ei2c_scl_port, &GPIO_InitStruct);

	/* set address */
	slaveAddr = addr;

	/* set speed */
	if (speed == 10) {
		ei2c_speed = EI2C_SPEED_LOW;
	} else if (speed == 400) {
		ei2c_speed = EI2C_SPEED_HIGH;
	} else {
		ei2c_speed = EI2C_SPEED_STANDARD;
	}

	return 0;
}

inline void ei2c_sda(uint8_t state)
{
	HAL_GPIO_WritePin(ei2c_sda_port, ei2c_sda_pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

inline void ei2c_scl(uint8_t state)
{
	HAL_GPIO_WritePin(ei2c_scl_port, ei2c_scl_pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void ei2c_sdaOut(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = ei2c_sda_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ei2c_sda_port, &GPIO_InitStruct);
}

void ei2c_sdaIn(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = ei2c_sda_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ei2c_sda_port, &GPIO_InitStruct);
}

inline uint8_t ei2c_getSda(void)
{
	return HAL_GPIO_ReadPin(ei2c_sda_port, ei2c_sda_pin);
}

///////////////////////////////////////////////////////////////////

void ei2c_delayUs(uint32_t uscount)
{
	volatile uint32_t t = uscount;
	volatile uint32_t x;
	volatile uint32_t y;
	for (x = 0; x < t; x++) {
		for (y = 0; y < ei2c_speed; y++) {
			x = x;
		}
	}
}

static void ei2c_start(void)
{
	ei2c_sda(1);
	ei2c_scl(1);
	ei2c_delayUs(4);

	ei2c_sda(0);
	ei2c_delayUs(4);

	ei2c_scl(0);
}

static void ei2c_stop(void)
{
	ei2c_sda(0);
	ei2c_delayUs(2);

	ei2c_scl(1);
	ei2c_delayUs(10);

	ei2c_sda(1);
	ei2c_delayUs(10);
}

static void ei2c_sendByte(uint8_t c)
{
	uint8_t bit_Cnt;

	for (bit_Cnt = 0; bit_Cnt < 8; bit_Cnt++) {
		if ((c << bit_Cnt) & 0x80) {
			ei2c_sda(1);
		} else {
			ei2c_sda(0);
		}
		ei2c_delayUs(3);
		ei2c_scl(1);
		ei2c_delayUs(4);
		ei2c_scl(0);
	}

	ei2c_delayUs(1);
	ei2c_sda(1);
	ei2c_delayUs(3);
	ei2c_scl(1);
	ei2c_delayUs(3);

	if (ei2c_getSda() != 0) {
		ack = 0;
	} else {
		ack = 1;
	}
	ei2c_scl(0);
	ei2c_delayUs(3);
}

static uint8_t ei2c_rcvByte(void)
{
	uint8_t retc;
	uint8_t bit_Cnt;

	retc = 0;
	ei2c_sda(1);
	for (bit_Cnt = 0; bit_Cnt < 8; bit_Cnt++) {
		ei2c_delayUs(1);
		ei2c_scl(0);
		ei2c_delayUs(4);
		ei2c_scl(1);
		ei2c_delayUs(2);
		retc = retc << 1;
		if (ei2c_getSda() != 0) {
			retc = retc + 1;
		}
		ei2c_delayUs(2);
	}
	ei2c_scl(0);
	ei2c_delayUs(5);
	return retc;
}

static void ei2c_ack(uint8_t a)
{
	if (a == 0) {
		ei2c_sda(0);
	} else {
		ei2c_sda(1);
	}

	ei2c_delayUs(8);
	ei2c_scl(1);
	ei2c_delayUs(15);
	ei2c_scl(0);
	ei2c_delayUs(8);
}

////////////////////////////////////////////////////////////////////
// ei2c_writeByte
static uint8_t ei2c_memWrite(uint8_t sla, uint8_t suba, uint8_t *s, uint8_t no)
{
	uint8_t i;

	ei2c_start();

	ei2c_sendByte(sla);

	if (ack == 0) {
		ei2c_stop();
		return FALSE;
	}
	ei2c_sendByte(suba);
	if (ack == 0) {
		ei2c_stop();
		return FALSE;
	}

	for (i = 0; i < no; i++) {
		ei2c_sendByte(*s);
		if (ack == 0) {
			ei2c_stop();
			return FALSE;
		}
		s++;
	}
	ei2c_stop();
	return TRUE;
}

// ei2c_readByte
static uint8_t ei2c_memRead(uint8_t sla, uint8_t suba, uint8_t *s, uint8_t no)
{
	uint8_t i;
	ei2c_start();
	ei2c_sendByte( (sla<<1) + 0x01);
	if (ack == 0) {
		ei2c_stop();
		*s = 0x00;
		return FALSE;
	}
	ei2c_sendByte(suba);
	if (ack == 0) {
		ei2c_stop();
		*s = 0x00;
		return FALSE;
	}
	ei2c_start();
	ei2c_sendByte(sla + 1);
	if (ack == 0) {
		ei2c_stop();
		*s = 0x00;
		return FALSE;
	}
	for (i = 0; i < no - 1; i++) {
		*s = ei2c_rcvByte();
		ei2c_ack(0);
		s++;
	}
	*s = ei2c_rcvByte();
	ei2c_ack(1);
	ei2c_stop();
	return TRUE;
}

/////////////////////////////////////////////////////////////////////
// https://blog.csdn.net/u013654125/article/details/78860338

// HDMI_ReadI2C_Byte
uint8_t ei2c_read1Byte(uint8_t RegAddr)
{
	uint8_t p_data = 0;

	if (ei2c_memRead(slaveAddr, RegAddr, &p_data, 1) != FALSE) {
		return p_data;
	}
	return 0;
}

int lua_ei2c_read1Byte(lua_State *L)
{
	int r = ei2c_read1Byte(lua_tointeger(L, 1));
	lua_pushinteger(L, r);
	return 1;
}

int lua_ei2c_readNByte(lua_State *L)
{
	uint8_t reg = lua_tointeger(L, 1);
	uint8_t cnt = lua_tointeger(L, 2);
	if (cnt > 64) {
		printf("[I2C] ERRPR cannot read more than 64\r\n");
	}

	uint8_t data[64];
	ei2c_memRead(slaveAddr, reg, data, cnt);
	for (uint8_t i = 0; i < cnt; i++) {
		lua_pushinteger(L, data[i]);
	}
	return cnt;
}

// HDMI_WriteI2C_Byte
uint8_t ei2c_write1Byte(uint8_t RegAddr, uint8_t d)
{
	uint8_t flag;

	flag = ei2c_memWrite(slaveAddr, RegAddr, &d, 1);

	return flag;
}

int lua_ei2c_write1Byte(lua_State *L)
{
	int reg = lua_tointeger(L, 1);
	int val = lua_tointeger(L, 2);
	ei2c_write1Byte(reg, val);
	return 0;
}
// HDMI_WriteI2C_ByteN
uint8_t ei2c_writeNByte(uint8_t RegAddr, uint8_t *d, uint8_t N)
{
	uint8_t flag;

	flag = ei2c_memWrite(slaveAddr, RegAddr, d, N);

	return flag;
}

int lua_ei2c_writeNByte(lua_State *L)
{
	int reg = lua_tointeger(L, 1);
	int cnt = lua_tointeger(L, 2);
	uint8_t buff[64];
	for (uint8_t i = 0; i < cnt; i++) {
		buff[i] = lua_tointeger(L, i + 3);
	}

	// ei2c_write1Byte(reg, val);
	return ei2c_writeNByte(reg, buff, cnt);
}

struct luaL_Reg functionExport_Bus[] = {
    {"ei2c_init", lua_ei2c_init},
    {"ei2c_read1Byte", lua_ei2c_read1Byte},
    {"ei2c_write1Byte", lua_ei2c_write1Byte},
    {"ei2c_readNByte", lua_ei2c_readNByte},
    {"ei2c_writeNByte", lua_ei2c_writeNByte},
    {NULL, NULL}};
