#ifndef __BUS_H__
#define __BUS_H__

#include "main.h"
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

extern struct luaL_Reg functionExport_Bus[];

#endif //__BUS_H__