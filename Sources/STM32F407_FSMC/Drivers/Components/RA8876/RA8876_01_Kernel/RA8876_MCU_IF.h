
/*************** RAIO Technology Inc. ***************
  * file		: RA8876_MCU_IF.h
  * author		: RAIO Application Team 
  * version		: V1.0 
  * date		: 2015/09/24  
  * brief		: 	
  ****************************************************/

#ifndef __RA8876_MCU_IF_H
#define __RA8876_MCU_IF_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "RA8876_UserDef.h"

typedef __IO uint16_t vu16;

/** @defgroup STM324xG_EVAL_LCD_Private_Defines
  * @{
  */
/* Note: LCD /CS is NE4 - Bank 4 of NOR/SRAM Bank 1~4 */

#ifdef Parallel_8080
//#define LCD_BASE0        ((uint32_t)(0x60000000 | 0x0C000000))
//#define LCD_BASE1        (LCD_BASE0+2)
//#define LCD_BASE0 ((uint32_t)(0x60000000))
//#define LCD_BASE1 ((uint32_t)(0x60080000))
#define LCD_BASE0 0x60000000
#define LCD_BASE1 0x60080000

#define LCD_CmdWrite(cmd) (*(vu16 *)(LCD_BASE0) = (cmd))
#define LCD_DataWrite(data) (*(vu16 *)(LCD_BASE1) = (data))
#define LCD_StatusRead() *(vu16 *)(LCD_BASE0) //if use read  Mcu interface DB0~DB15 needs increase pull high
#define LCD_DataRead() *(vu16 *)(LCD_BASE1)   //if use read  Mcu interface DB0~DB15 needs increase pull high
#endif

void LCD_CtrlLinesConfig(void);
void LCD_FSMCConfig(void);

#endif /*__RA8876_MCU_IF_H*/
