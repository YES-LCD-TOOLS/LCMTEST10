
/*************** RAIO Technology Inc. ***************
  * file		: RA8876_API.c
  * author		: RAIO Application Team ^v^ 
  * version		: V1.0  
  * date		: 2014/06/17 
  * brief		: 	
  ****************************************************/
#include "RA8876_API.h"
#include "RA8876.h"
#include "RA8876_MCU_IF.h"
#include "RA8876_UserDef.h"
#include "ascii_table_16x24.h"
#include "ascii_table_32x48.h"
#include "ascii_table_8x12.h"
#include "stm32f4xx.h"
#include <stdio.h>
#include <stdlib.h>
char tmp1[32];
void RA8876_MPU8_8bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data)
{
	uint16_t i, j;
	Graphic_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(w, h);
	Goto_Pixel_XY(x, y);
	LCD_CmdWrite(0x04);
	for (i = 0; i < h; i++) {
		for (j = 0; j < w; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
}
void RA8876_MPU8_16bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data)
{
	uint16_t i, j;
	Graphic_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(w, h);
	Goto_Pixel_XY(x, y);
	LCD_CmdWrite(0x04);
	for (i = 0; i < h; i++) {
		for (j = 0; j < w; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
}
void RA8876_MPU8_24bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data)
{
	uint16_t i, j;
	Graphic_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(w, h);
	Goto_Pixel_XY(x, y);
	LCD_CmdWrite(0x04);
	for (i = 0; i < h; i++) {
		for (j = 0; j < w; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
}
void RA8876_MPU16_16bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data)
{
	uint16_t i, j;
	Graphic_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(w, h);
	Goto_Pixel_XY(x, y);
	LCD_CmdWrite(0x04);
	for (i = 0; i < h; i++) {
		for (j = 0; j < w; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
}
void RA8876_MPU16_24bpp_Mode1_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data)
{
	uint16_t i, j;
	Graphic_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(w, h);
	Goto_Pixel_XY(x, y);
	LCD_CmdWrite(0x04);
	for (i = 0; i < h; i++) {
		for (j = 0; j < w / 2; j++) {
			LCD_DataWrite(*data);
			Check_Mem_WR_FIFO_not_Full();
			data++;
			LCD_DataWrite(*data);
			Check_Mem_WR_FIFO_not_Full();
			data++;
			LCD_DataWrite(*data);
			Check_Mem_WR_FIFO_not_Full();
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
}
void RA8876_MPU16_24bpp_Mode2_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data)
{
	uint16_t i, j;
	Graphic_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(w, h);
	Goto_Pixel_XY(x, y);
	LCD_CmdWrite(0x04);
	for (i = 0; i < h; i++) {
		for (j = 0; j < w; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
}
void RA8876_PIP(uint8_t On_Off, uint8_t Select_PIP, uint32_t PAddr, uint16_t XP, uint16_t YP, uint32_t ImageWidth, uint16_t X_Dis, uint16_t Y_Dis, uint16_t X_W, uint16_t Y_H)
{
	if (Select_PIP == 1) {
		Select_PIP1_Parameter();
	}
	if (Select_PIP == 2) {
		Select_PIP2_Parameter();
	}
	PIP_Display_Start_XY(X_Dis, Y_Dis);
	PIP_Image_Start_Address(PAddr);
	PIP_Image_Width(ImageWidth);
	PIP_Window_Image_Start_XY(XP, YP);
	PIP_Window_Width_Height(X_W, Y_H);
	if (On_Off == 0) {
		if (Select_PIP == 1) {
			Disable_PIP1();
		}
		if (Select_PIP == 2) {
			Disable_PIP2();
		}
	}
	if (On_Off == 1) {
		if (Select_PIP == 1) {
			Enable_PIP1();
		}
		if (Select_PIP == 2) {
			Enable_PIP2();
		}
	}
}
void RA8876_Print_Internal_Font_Hexvariable(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, unsigned int tmp2)
{
	Text_Mode();
	CGROM_Select_Internal_CGROM();
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(FontColor);
	Background_color_256(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
	Active_Window_XY(x, y);
	Active_Window_WH(X_W, Y_H);
	Goto_Text_XY(x, y);
	sprintf(tmp1, "%X", tmp2);
	RA8876_Show_String(tmp1);
}
void RA8876_Print_Internal_Font_Decimalvariable(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, unsigned int tmp2)
{
	Text_Mode();
	CGROM_Select_Internal_CGROM();
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(FontColor);
	Background_color_256(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
	Active_Window_XY(x, y);
	Active_Window_WH(X_W, Y_H);
	Goto_Text_XY(x, y);
	sprintf(tmp1, "%d", tmp2);
	RA8876_Show_String(tmp1);
}
void RA8876_Print_Internal_Font_String(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char tmp2[])
{
	Text_Mode();
	CGROM_Select_Internal_CGROM();
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(FontColor);
	Background_color_256(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
	Active_Window_XY(x, y);
	Active_Window_WH(X_W, Y_H);
	Goto_Text_XY(x, y);
	RA8876_Show_String(tmp2);
}
void RA8876_Print_BIG5String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char *tmp2)
{
	Select_SFI_Font_Mode();
	CGROM_Select_Genitop_FontROM();
	SPI_Clock_Period(Clk);
	Set_GTFont_Decoder(0x11);
	if (SCS == 0) {
		Select_SFI_0();
	}
	if (SCS == 1) {
		Select_SFI_1();
	}
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(FontColor);
	Background_color_256(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
	Text_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(X_W, Y_H);
	Goto_Text_XY(x, y);
	RA8876_Show_String(tmp2);
}
void RA8876_Print_GB2312String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char tmp2[])
{
	Select_SFI_Font_Mode();
	CGROM_Select_Genitop_FontROM();
	SPI_Clock_Period(Clk);
	Set_GTFont_Decoder(0x01);
	if (SCS == 0) {
		Select_SFI_0();
	}
	if (SCS == 1) {
		Select_SFI_1();
	}
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(FontColor);
	Background_color_256(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
	Text_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(X_W, Y_H);
	Goto_Text_XY(x, y);
	RA8876_Show_String(tmp2);
}
void RA8876_Print_GB12345String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char *tmp2)
{
	Select_SFI_Font_Mode();
	CGROM_Select_Genitop_FontROM();
	SPI_Clock_Period(Clk);
	Set_GTFont_Decoder(0x05);
	if (SCS == 0) {
		Select_SFI_0();
	}
	if (SCS == 1) {
		Select_SFI_1();
	}
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(FontColor);
	Background_color_256(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
	Text_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(X_W, Y_H);
	Goto_Text_XY(x, y);
	RA8876_Show_String(tmp2);
}
void RA8876_Print_UnicodeString(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, uint16_t *tmp2)
{
	Select_SFI_Font_Mode();
	CGROM_Select_Genitop_FontROM();
	SPI_Clock_Period(Clk);
	if (SCS == 0) {
		Select_SFI_0();
	}
	if (SCS == 1) {
		Select_SFI_1();
	}
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(FontColor);
	Background_color_256(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(FontColor);
	Background_color_65k(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(FontColor);
	Background_color_16M(BackGroundColor);
#endif
	Text_Mode();
	Active_Window_XY(x, y);
	Active_Window_WH(X_W, Y_H);
	Goto_Text_XY(x, y);
	while (*tmp2 != '\0') {
		if ((*tmp2) > 0x0020 && (*tmp2) < 0x0080) { /* ASCII Code*/
			Set_GTFont_Decoder(0x21);
			LCD_CmdWrite(0x04);
			LCD_DataWrite(*tmp2);
			Check_2D_Busy();
		} else { /* Unicode */
			Set_GTFont_Decoder(0x18);
			LCD_CmdWrite(0x04);
			LCD_DataWrite((*tmp2) >> 8);
			LCD_DataWrite(*tmp2);
			Check_2D_Busy();
		}
		++tmp2;
	}
	Graphic_Mode();
}
void RA8876_Select_Font_Height_WxN_HxN_ChromaKey_Alignment(uint8_t Font_Height, uint8_t XxN, uint8_t YxN, uint8_t ChromaKey, uint8_t Alignment)
{
	if (Font_Height == 16)
		Font_Select_8x16_16x16();
	if (Font_Height == 24)
		Font_Select_12x24_24x24();
	if (Font_Height == 32)
		Font_Select_16x32_32x32();
	if (XxN == 1)
		Font_Width_X1();
	if (XxN == 2)
		Font_Width_X2();
	if (XxN == 3)
		Font_Width_X3();
	if (XxN == 4)
		Font_Width_X4();
	if (YxN == 1)
		Font_Height_X1();
	if (YxN == 2)
		Font_Height_X2();
	if (YxN == 3)
		Font_Height_X3();
	if (YxN == 4)
		Font_Height_X4();
	if (ChromaKey == 0) {
		Font_Background_select_Color();
	}
	if (ChromaKey == 1) {
		Font_Background_select_Original_Canvas();
	}
	if (Alignment == 0) {
		Disable_Font_Alignment();
	}
	if (Alignment == 1) {
		Enable_Font_Alignment();
	}
}
void RA8876_Show_String(char *str)
{
	Check_Mem_WR_FIFO_Empty();
	Text_Mode();
	LCD_CmdWrite(0x04);
	while (*str != '\0') {
		Check_Mem_WR_FIFO_not_Full();
		LCD_DataWrite(*str);
		++str;
	}
	Check_2D_Busy();
	Graphic_Mode();
}
void RA8876_Draw_Line(uint32_t LineColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(LineColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(LineColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(LineColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(LineColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(LineColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(LineColor);
#endif
	Line_Start_XY(X1, Y1);
	Line_End_XY(X2, Y2);
	Start_Line();
	Check_2D_Busy();
}
void RA8876_Draw_Triangle(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X3, uint16_t Y3)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Triangle_Point1_XY(X1, Y1);
	Triangle_Point2_XY(X2, Y2);
	Triangle_Point3_XY(X3, Y3);
	Start_Triangle();
	Check_2D_Busy();
}
void RA8876_Draw_Triangle_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X3, uint16_t Y3)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Triangle_Point1_XY(X1, Y1);
	Triangle_Point2_XY(X2, Y2);
	Triangle_Point3_XY(X3, Y3);
	Start_Triangle_Fill();
	Check_2D_Busy();
}
void RA8876_Draw_Circle(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Circle_Center_XY(XCenter, YCenter);
	Circle_Radius_R(R);
	Start_Circle_or_Ellipse();
	Check_2D_Busy();
}
void RA8876_Draw_Circle_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Circle_Center_XY(XCenter, YCenter);
	Circle_Radius_R(R);
	Start_Circle_or_Ellipse_Fill();
	Check_2D_Busy();
}
void RA8876_Draw_Ellipse(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Circle_or_Ellipse();
	Check_2D_Busy();
}
void RA8876_Draw_Ellipse_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Circle_or_Ellipse_Fill();
	Check_2D_Busy();
}
void RA8876_Draw_Left_Up_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Left_Up_Curve();
	Check_2D_Busy();
}
void RA8876_Draw_Left_Up_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Left_Up_Curve_Fill();
	Check_2D_Busy();
}
void RA8876_Draw_Right_Down_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Right_Down_Curve();
	Check_2D_Busy();
}
void RA8876_Draw_Right_Down_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Right_Down_Curve_Fill();
	Check_2D_Busy();
}
void RA8876_Draw_Right_Up_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Right_Up_Curve();
	Check_2D_Busy();
}
void RA8876_Draw_Right_Up_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Right_Up_Curve_Fill();
	Check_2D_Busy();
}
void RA8876_Draw_Left_Down_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Left_Down_Curve();
	Check_2D_Busy();
}
void RA8876_Draw_Left_Down_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Ellipse_Center_XY(XCenter, YCenter);
	Ellipse_Radius_RxRy(X_R, Y_R);
	Start_Left_Down_Curve_Fill();
	Check_2D_Busy();
}
void RA8876_Draw_Square(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Square_Start_XY(X1, Y1);
	Square_End_XY(X2, Y2);
	Start_Square();
	Check_2D_Busy();
}
void RA8876_Draw_Square_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Square_Start_XY(X1, Y1);
	Square_End_XY(X2, Y2);
	Start_Square_Fill();
	Check_2D_Busy();
}
void RA8876_Draw_Circle_Square(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Square_Start_XY(X1, Y1);
	Square_End_XY(X2, Y2);
	Circle_Square_Radius_RxRy(X_R, Y_R);
	Start_Circle_Square();
	Check_2D_Busy();
}
void RA8876_Draw_Circle_Square_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X_R, uint16_t Y_R)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(ForegroundColor);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(ForegroundColor);
#endif
	Square_Start_XY(X1, Y1);
	Square_End_XY(X2, Y2);
	Circle_Square_Radius_RxRy(X_R, Y_R);
	Start_Circle_Square_Fill();
	Check_2D_Busy();
}
void RA8876_BTE_Pattern_Fill(uint8_t P_8x8_or_16x16, uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H)
{
	if (P_8x8_or_16x16 == 0) {
		Pattern_Format_8X8();
	}
	if (P_8x8_or_16x16 == 1) {
		Pattern_Format_16X16();
	}
	BTE_S0_Memory_Start_Address(S0_Addr);
	BTE_S0_Image_Width(S0_W);
	BTE_S0_Window_Start_XY(XS0, YS0);
	BTE_S1_Memory_Start_Address(S1_Addr);
	BTE_S1_Image_Width(S1_W);
	BTE_S1_Window_Start_XY(XS1, YS1);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_ROP_Code(ROP_Code);
	BTE_Operation_Code(0x06);
	BTE_Window_Size(X_W, Y_H);
	BTE_Enable();
	Check_BTE_Busy();
}
void RA8876_BTE_Pattern_Fill_With_Chroma_key(uint8_t P_8x8_or_16x16, uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint32_t Background_color, uint16_t X_W, uint16_t Y_H)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Background_color_256(Background_color);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Background_color_65k(Background_color);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Background_color_16M(Background_color);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Background_color_65k(Background_color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Background_color_16M(Background_color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Background_color_16M(Background_color);
#endif
	if (P_8x8_or_16x16 == 0) {
		Pattern_Format_8X8();
	}
	if (P_8x8_or_16x16 == 1) {
		Pattern_Format_16X16();
	}
	BTE_S0_Memory_Start_Address(S0_Addr);
	BTE_S0_Image_Width(S0_W);
	BTE_S0_Window_Start_XY(XS0, YS0);
	BTE_S1_Memory_Start_Address(S1_Addr);
	BTE_S1_Image_Width(S1_W);
	BTE_S1_Window_Start_XY(XS1, YS1);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_ROP_Code(ROP_Code);
	BTE_Operation_Code(0x07);
	BTE_Window_Size(X_W, Y_H);
	BTE_Enable();
	Check_BTE_Busy();
}
void RA8876_BTE_Memory_Copy(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H)
{
	BTE_S0_Memory_Start_Address(S0_Addr);
	BTE_S0_Image_Width(S0_W);
	BTE_S0_Window_Start_XY(XS0, YS0);
	BTE_S1_Memory_Start_Address(S1_Addr);
	BTE_S1_Image_Width(S1_W);
	BTE_S1_Window_Start_XY(XS1, YS1);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_ROP_Code(ROP_Code);
	BTE_Operation_Code(0x02);
	BTE_Window_Size(X_W, Y_H);
	BTE_Enable();
	Check_BTE_Busy();
}
void RA8876_BTE_Memory_Copy_Chroma_key(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Background_color_256(Background_color);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Background_color_65k(Background_color);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Background_color_16M(Background_color);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Background_color_65k(Background_color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Background_color_16M(Background_color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Background_color_16M(Background_color);
#endif
	BTE_S0_Memory_Start_Address(S0_Addr);
	BTE_S0_Image_Width(S0_W);
	BTE_S0_Window_Start_XY(XS0, YS0);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Operation_Code(0x05);
	BTE_Window_Size(X_W, Y_H);
	BTE_Enable();
	Check_BTE_Busy();
}
void RA8876_BTE_MCU_Write_MCU_8bit(uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H, const uint8_t *data)
{
	uint16_t i, j;
	BTE_S1_Memory_Start_Address(S1_Addr);
	BTE_S1_Image_Width(S1_W);
	BTE_S1_Window_Start_XY(XS1, YS1);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_ROP_Code(ROP_Code);
	BTE_Operation_Code(0x00);
	BTE_Enable();
	LCD_CmdWrite(0x04);
#ifdef MCU_8bit_ColorDepth_8bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W * 2); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W * 3); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
}
void RA8876_BTE_MCU_Write_MCU_16bit(uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H, const uint16_t *data)
{
	uint16_t i = 0;
	uint16_t j = 0;
	i = i;
	j = j;
	BTE_S1_Memory_Start_Address(S1_Addr);
	BTE_S1_Image_Width(S1_W);
	BTE_S1_Window_Start_XY(XS1, YS1);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_ROP_Code(ROP_Code);
	BTE_Operation_Code(0x00);
	BTE_Enable();
	LCD_CmdWrite(0x04);
#ifdef MCU_16bit_ColorDepth_16bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite((*data));
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W / 2); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
	Check_BTE_Busy();
}
void RA8876_BTE_MCU_Write_Chroma_key_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H, const uint8_t *data)
{
	uint16_t i, j;
#ifdef MCU_8bit_ColorDepth_8bpp
	Background_color_256(Background_color);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Background_color_65k(Background_color);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Background_color_16M(Background_color);
#endif
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0x04);
	BTE_Enable();
	LCD_CmdWrite(0x04);
#ifdef MCU_8bit_ColorDepth_8bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W * 2); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W * 3); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
	Check_BTE_Busy();
}
void RA8876_BTE_MCU_Write_Chroma_key_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H, const uint16_t *data)
{
	unsigned int i = 0, j = 0;
	i = i;
	j = j;
#ifdef MCU_16bit_ColorDepth_16bpp
	Background_color_65k(Background_color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Background_color_16M(Background_color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Background_color_16M(Background_color);
#endif
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0x04);
	BTE_Enable();
	LCD_CmdWrite(0x04);
#ifdef MCU_16bit_ColorDepth_16bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W / 2); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < (X_W); j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
	Check_BTE_Busy();
}
void RA8876_BTE_Memory_Copy_ColorExpansion(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(Foreground_color);
	Background_color_256(Background_color);
	BTE_ROP_Code(7);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(Foreground_color);
	Background_color_65k(Background_color);
	BTE_ROP_Code(15);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(Foreground_color);
	Background_color_65k(Background_color);
	BTE_ROP_Code(15);
#endif
	BTE_S0_Memory_Start_Address(S0_Addr);
	BTE_S0_Image_Width(S0_W);
	BTE_S0_Window_Start_XY(XS0, YS0);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0xE);
#ifdef MCU_8bit_ColorDepth_8bpp
	BTE_Enable();
	Check_BTE_Busy();
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	BTE_Enable();
	Check_BTE_Busy();
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	BTE_Enable();
	Check_BTE_Busy();
#endif
}
void RA8876_BTE_Memory_Copy_ColorExpansion_Chroma_key(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(Foreground_color);
	BTE_ROP_Code(7);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(Foreground_color);
	BTE_ROP_Code(15);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(Foreground_color);
	BTE_ROP_Code(15);
#endif
	BTE_S0_Memory_Start_Address(S0_Addr);
	BTE_S0_Image_Width(S0_W);
	BTE_S0_Window_Start_XY(XS0, YS0);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0xF);
#ifdef MCU_8bit_ColorDepth_8bpp
	BTE_Enable();
	Check_BTE_Busy();
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	BTE_Enable();
	Check_BTE_Busy();
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	BTE_Enable();
	Check_BTE_Busy();
#endif
}
void RA8876_BTE_MCU_Write_ColorExpansion_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color, const uint8_t *data)
{
	uint16_t i, j;
	/* set BTE Parameters and Run */
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(Foreground_color);
	Background_color_256(Background_color);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(Foreground_color);
	Background_color_65k(Background_color);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(Foreground_color);
	Background_color_16M(Background_color);
#endif
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_ROP_Code(7);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0x8);
	BTE_Enable();
	LCD_CmdWrite(0x04);
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W / 8; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
	Check_BTE_Busy();
}
void RA8876_BTE_MCU_Write_ColorExpansion_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color, const uint16_t *data)
{
	uint16_t i = 0, j = 0;
	i = i;
	j = j;
#ifdef MCU_16bit_ColorDepth_16bpp
	Data_Format_16b_16bpp();
	Foreground_color_65k(Foreground_color);
	Background_color_65k(Background_color);
	BTE_ROP_Code(15);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Data_Format_16b_8bpp();
	Foreground_color_16M(Foreground_color);
	Background_color_16M(Background_color);
	BTE_ROP_Code(7);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Data_Format_16b_8bpp();
	Foreground_color_16M(Foreground_color);
	Background_color_16M(Background_color);
	BTE_ROP_Code(7);
#endif
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0x8);
	BTE_Enable();
#ifdef MCU_16bit_ColorDepth_16bpp
	LCD_CmdWrite(0x04);
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W / 16; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	LCD_CmdWrite(0x04);
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W / 16; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data >> 8);
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	LCD_CmdWrite(0x04);
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W / 16; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data >> 8);
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
	Check_BTE_Busy();
#ifdef MCU_16bit_ColorDepth_16bpp
	Data_Format_16b_16bpp();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Data_Format_16b_24bpp_mode1();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Data_Format_16b_24bpp_mode2();
#endif
}
void RA8876_BTE_MCU_Write_ColorExpansion_Chroma_key_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, const uint8_t *data)
{
	uint16_t i, j;
	/* set BTE Parameters and Run */
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(Foreground_color);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(Foreground_color);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(Foreground_color);
#endif
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_ROP_Code(7);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0x9);
	BTE_Enable();
	LCD_CmdWrite(0x04);
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W / 8; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
	Check_BTE_Busy();
}
void RA8876_BTE_MCU_Write_ColorExpansion_Chroma_key_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, const uint16_t *data)
{
	uint16_t i = 0, j = 0;
	i = i;
	j = j;
#ifdef MCU_16bit_ColorDepth_16bpp
	Data_Format_16b_16bpp();
	Foreground_color_65k(Foreground_color);
	BTE_ROP_Code(15);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Data_Format_16b_8bpp();
	Foreground_color_16M(Foreground_color);
	BTE_ROP_Code(7);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Data_Format_16b_8bpp();
	Foreground_color_16M(Foreground_color);
	BTE_ROP_Code(7);
#endif
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0x9);
	BTE_Enable();
	LCD_CmdWrite(0x04);
#ifdef MCU_16bit_ColorDepth_16bpp
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W / 16; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W / 16; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data >> 8);
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	for (i = 0; i < Y_H; i++) {
		for (j = 0; j < X_W / 16; j++) {
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data >> 8);
			Check_Mem_WR_FIFO_not_Full();
			LCD_DataWrite(*data);
			data++;
		}
	}
	Check_Mem_WR_FIFO_Empty();
#endif
	Check_BTE_Busy();
}
void RA8876_BTE_Solid_Fill(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Foreground_color, uint16_t X_W, uint16_t Y_H)
{
#ifdef MCU_8bit_ColorDepth_8bpp
	Foreground_color_256(Foreground_color);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	Foreground_color_65k(Foreground_color);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	Foreground_color_16M(Foreground_color);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	Foreground_color_65k(Foreground_color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_1
	Foreground_color_16M(Foreground_color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	Foreground_color_16M(Foreground_color);
#endif
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0x0c);
	BTE_Enable();
	Check_BTE_Busy();
}
void RA8876_BTE_Alpha_Blending(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint8_t alpha)
{
	BTE_S0_Memory_Start_Address(S0_Addr);
	BTE_S0_Image_Width(S0_W);
	BTE_S0_Window_Start_XY(XS0, YS0);
	BTE_S1_Memory_Start_Address(S1_Addr);
	BTE_S1_Image_Width(S1_W);
	BTE_S1_Window_Start_XY(XS1, YS1);
	BTE_Destination_Memory_Start_Address(Des_Addr);
	BTE_Destination_Image_Width(Des_W);
	BTE_Destination_Window_Start_XY(XDes, YDes);
	BTE_Window_Size(X_W, Y_H);
	BTE_Operation_Code(0x0A);
	BTE_Alpha_Blending_Effect(alpha);
	BTE_Enable();
	Check_BTE_Busy();
}
void RA8876_DMA_24bit(uint8_t SCS, uint8_t Clk, uint16_t X1, uint16_t Y1, uint16_t X_W, uint16_t Y_H, uint16_t P_W, uint32_t Addr)
{
	if (SCS == 0) {
		Select_SFI_0();
	}
	if (SCS == 1) {
		Select_SFI_1();
	}
	Select_SFI_DMA_Mode();
	SPI_Clock_Period(Clk);
	Goto_Pixel_XY(X1, Y1);
	SFI_DMA_Destination_Upper_Left_Corner(X1, Y1);
	SFI_DMA_Transfer_Width_Height(X_W, Y_H);
	SFI_DMA_Source_Width(P_W);
	SFI_DMA_Source_Start_Address(Addr);
	Start_SFI_DMA();
	Check_Busy_SFI_DMA();
}
void RA8876_DMA_32bit(uint8_t SCS, uint8_t Clk, uint16_t X1, uint16_t Y1, uint16_t X_W, uint16_t Y_H, uint16_t P_W, uint32_t Addr)
{
	if (SCS == 0) {
		Select_SFI_0();
	}
	if (SCS == 1) {
		Select_SFI_1();
	}
	Select_SFI_DMA_Mode();
	SPI_Clock_Period(Clk);
	Select_SFI_32bit_Address();
	Goto_Pixel_XY(X1, Y1);
	SFI_DMA_Destination_Upper_Left_Corner(X1, Y1);
	SFI_DMA_Transfer_Width_Height(X_W, Y_H);
	SFI_DMA_Source_Width(P_W);
	SFI_DMA_Source_Start_Address(Addr);
	Start_SFI_DMA();
	Check_Busy_SFI_DMA();
	Select_SFI_24bit_Address();
}
void RA8876_switch_24bits_to_32bits(uint8_t SCS)
{
	if (SCS == 0) {
		Select_nSS_drive_on_xnsfcs0();
	}
	if (SCS == 1) {
		Select_nSS_drive_on_xnsfcs1();
	}
	Reset_CPOL();
	Reset_CPHA();
	nSS_Active();
	SPI_Master_FIFO_Data_Put(0xB7);
	HAL_Delay(1);
	nSS_Inactive();
}
void RA8876_PWM0(uint8_t on_off, uint8_t Clock_Divided, uint8_t Prescalar, uint16_t Count_Buffer, uint16_t Compare_Buffer)
{
	Select_PWM0();
	Set_PWM_Prescaler_1_to_256(Prescalar);
	if (Clock_Divided == 0) {
		Select_PWM0_Clock_Divided_By_1();
	}
	if (Clock_Divided == 1) {
		Select_PWM0_Clock_Divided_By_2();
	}
	if (Clock_Divided == 2) {
		Select_PWM0_Clock_Divided_By_4();
	}
	if (Clock_Divided == 3) {
		Select_PWM0_Clock_Divided_By_8();
	}
	Set_Timer0_Count_Buffer(Count_Buffer);
	Set_Timer0_Compare_Buffer(Compare_Buffer);
	if (on_off == 1) {
		Start_PWM0();
	}
	if (on_off == 0) {
		Stop_PWM0();
	}
}
void RA8876_PWM1(uint8_t on_off, uint8_t Clock_Divided, uint8_t Prescalar, uint16_t Count_Buffer, uint16_t Compare_Buffer)
{
	Select_PWM1();
	Set_PWM_Prescaler_1_to_256(Prescalar);
	if (Clock_Divided == 0) {
		Select_PWM1_Clock_Divided_By_1();
	}
	if (Clock_Divided == 1) {
		Select_PWM1_Clock_Divided_By_2();
	}
	if (Clock_Divided == 2) {
		Select_PWM1_Clock_Divided_By_4();
	}
	if (Clock_Divided == 3) {
		Select_PWM1_Clock_Divided_By_8();
	}
	Set_Timer1_Count_Buffer(Count_Buffer);
	Set_Timer1_Compare_Buffer(Compare_Buffer);
	if (on_off == 1) {
		Start_PWM1();
	}
	if (on_off == 0) {
		Stop_PWM1();
	}
}
void RA8876_putPixel(uint16_t x, uint16_t y, uint32_t color)
{
	Goto_Pixel_XY(x, y);
	LCD_CmdWrite(0x04);
	Check_Mem_WR_FIFO_not_Full();
#ifdef MCU_8bit_ColorDepth_8bpp
	LCD_DataWrite(color);
#endif
#ifdef MCU_8bit_ColorDepth_16bpp
	LCD_DataWrite(color);
	Check_Mem_WR_FIFO_not_Full();
	LCD_DataWrite(color >> 8);
#endif
#ifdef MCU_8bit_ColorDepth_24bpp
	LCD_DataWrite(color);
	Check_Mem_WR_FIFO_not_Full();
	LCD_DataWrite(color >> 8);
	Check_Mem_WR_FIFO_not_Full();
	LCD_DataWrite(color >> 16);
#endif
#ifdef MCU_16bit_ColorDepth_16bpp
	LCD_DataWrite(color);
#endif
#ifdef MCU_16bit_ColorDepth_24bpp_Mode_2
	LCD_DataWrite(color);
	Check_Mem_WR_FIFO_not_Full();
	LCD_DataWrite(color >> 16);
#endif
}
void RA8876_lcdPutChar8x12(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code)
{
	uint16_t i = 0;
	uint16_t j = 0;
	uint8_t tmp_char = 0;
	for (i = 0; i < 12; i++) {
		tmp_char = ascii_table_8x12[((code - 0x20) * 12) + i];
		for (j = 0; j < 8; j++) {
			if ((tmp_char >> (7 - j)) & (0x01 == 0x01))
				RA8876_putPixel(x + j, y + i, fgcolor);
			else {
				if (!bg_transparent)
					RA8876_putPixel(x + j, y + i, bgcolor);
			}
		}
	}
}
void RA8876_lcdPutString8x12(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr)
{
	uint16_t i = 0;
	while ((*ptr != 0) & (i < 100)) {
		RA8876_lcdPutChar8x12(x, y, fgcolor, bgcolor, bg_transparent, *ptr);
		x += 8;
		ptr++;
		i++;
	}
}
void RA8876_lcdPutChar16x24(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code)
{
	uint16_t i = 0;
	uint16_t j = 0;
	uint32_t array_addr = 0;
	unsigned int tmp_char = 0;
	for (i = 0; i < 24; i++) {
		array_addr = ((code - 0x20) * 2 * 24) + (i * 2);
		tmp_char = ascii_table_16x24[array_addr] << 8 | ascii_table_16x24[array_addr + 1];
		for (j = 0; j < 16; j++) {
			if ((tmp_char >> (15 - j)) & (0x01 == 0x01))
				RA8876_putPixel(x + j, y + i, fgcolor);
			else {
				if (!bg_transparent)
					RA8876_putPixel(x + j, y + i, bgcolor);
			}
		}
	}
}
void RA8876_lcdPutString16x24(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr)
{
	uint16_t i = 0;
	while ((*ptr != 0) & (i < 50)) {
		RA8876_lcdPutChar16x24(x, y, fgcolor, bgcolor, bg_transparent, *ptr);
		x += 16;
		ptr++;
		i++;
	}
}
void RA8876_lcdPutChar32x48(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code)
{
	uint16_t i = 0;
	uint16_t j = 0;
	uint32_t array_addr = 0;
	uint32_t tmp_char = 0;
	for (i = 0; i < 48; i++) {
		array_addr = ((code - 0x20) * 4 * 48) + (i * 4);
		tmp_char = ascii_table_32x48[array_addr] << 24 | ascii_table_32x48[array_addr + 1] << 16 | ascii_table_32x48[array_addr + 2] << 8 | ascii_table_32x48[array_addr + 3];
		for (j = 0; j < 32; j++) {
			if ((tmp_char >> (31 - j)) & (0x01 == 0x01))
				RA8876_putPixel(x + j, y + i, fgcolor);
			else {
				if (!bg_transparent)
					RA8876_putPixel(x + j, y + i, bgcolor);
			}
		}
	}
}
void RA8876_lcdPutString32x48(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr)
{
	uint16_t i = 0;
	while ((*ptr != 0) & (i < 25)) {
		RA8876_lcdPutChar32x48(x, y, fgcolor, bgcolor, bg_transparent, *ptr);
		x += 32;
		ptr++;
		i++;
	}
}
