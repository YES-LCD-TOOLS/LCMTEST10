
/*************** RAIO Technology Inc. ***************
  * file		: RA8876_API.h
  * author		: RAIO Application Team ^v^ 
  * version		: V1.0  
  * date		: 2014/06/17 
  * brief		: 	
  ****************************************************/
#include <stdint.h>

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param w width
 * @param h height
 * @param data 8bit data
 */
void RA8876_MPU8_8bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param w width
 * @param h height
 * @param data 8bit data
 */
void RA8876_MPU8_16bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param w width
 * @param h height
 * @param data 8bit data
 */
void RA8876_MPU8_24bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *data);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param w width
 * @param h height
 * @param data 8bit data
 */
void RA8876_MPU16_16bpp_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param w width
 * @param h height
 * @param data 8bit data
 */
void RA8876_MPU16_24bpp_Mode1_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param w width
 * @param h height
 * @param data 8bit data
 */
void RA8876_MPU16_24bpp_Mode2_Memory_Write(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint16_t *data);

/**
 * @brief 
 * 
 * @param On_Off 0 : disable PIP, 1 : enable PIP, 2 : To maintain the original state
 * @param Select_PIP 1 : use PIP1 , 2 : use PIP2
 * @param PAddr start address of PIP
 * @param XP coordinate X of PIP Window, It must be divided by 4.
 * @param YP coordinate Y of PIP Window, It must be divided by 4.
 * @param ImageWidth Image Width of PIP (recommend = canvas image width)
 * @param X_Dis coordinate X of Display Window
 * @param Y_Dis coordinate Y of Display Window
 * @param X_W Width of BTE Winodwwidth of PIP and Display Window, It must be divided by 4.
 * @param Y_H height of PIP and Display Window , It must be divided by 4.
 */
void RA8876_PIP(uint8_t On_Off, uint8_t Select_PIP, uint32_t PAddr, uint16_t XP, uint16_t YP, uint32_t ImageWidth, uint16_t X_Dis, uint16_t Y_Dis, uint16_t X_W, uint16_t Y_H);

/**
 * @brief 
 * 
 * @param x Print font start coordinate of X
 * @param y Print font start coordinate of Y
 * @param X_W Width of BTE Winodwactive window width
 * @param Y_H active window height
 * @param FontColor Set Font Color
 * @param BackGroundColor Set Font BackGround Color 
 * @param tmp2 Hex variable you want print (range : 0 ~ 32bit)
 */
void RA8876_Print_Internal_Font_Hexvariable(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, unsigned int tmp2);

/**
 * @brief 
 * 
 * @param x Print font start coordinate of X
 * @param y Print font start coordinate of Y
 * @param X_W Width of BTE Winodwactive window width
 * @param Y_H active window height
 * @param FontColor Set Font Color
 * @param BackGroundColor Set Font BackGround Color 
 * @param tmp2 Hex variable you want print (range : 0 ~ 32bit)
 */
void RA8876_Print_Internal_Font_Decimalvariable(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, unsigned int tmp2);

/**
 * @brief 
 * 
 * @param x Print font start coordinate of X
 * @param y Print font start coordinate of Y
 * @param X_W Width of BTE Winodwactive window width
 * @param Y_H active window height
 * @param FontColor Set Font Color
 * @param BackGroundColor Set Font BackGround Color 
 * @param tmp2 Hex variable you want print (range : 0 ~ 32bit)
 */
void RA8876_Print_Internal_Font_String(uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char tmp2[]);

/**
 * @brief 
 * 
 * @param Clk SPI CLK = System Clock / 2*(Clk+1)
 * @param SCS 0 : use CS0 , 1 : use CS1
 * @param x Print font start coordinate of X
 * @param y Print font start coordinate of Y
 * @param X_W Width of BTE Winodwactive window width
 * @param Y_H active window height
 * @param FontColor Set Font Color
 * @param BackGroundColor Set Font BackGround Color 
 * @param tmp2 Hex variable you want print (range : 0 ~ 32bit)
 */
void RA8876_Print_BIG5String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char *tmp2);

/**
 * @brief 
 * 
 * @param Clk SPI CLK = System Clock / 2*(Clk+1)
 * @param SCS 0 : use CS0 , 1 : use CS1
 * @param x Print font start coordinate of X
 * @param y Print font start coordinate of Y
 * @param X_W Width of BTE Winodwactive window width
 * @param Y_H active window height
 * @param FontColor Set Font Color
 * @param BackGroundColor Set Font BackGround Color 
 * @param tmp2 Hex variable you want print (range : 0 ~ 32bit)
 */
void RA8876_Print_GB2312String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char *tmp2);

/**
 * @brief 
 * 
 * @param Clk SPI CLK = System Clock / 2*(Clk+1)
 * @param SCS 0 : use CS0 , 1 : use CS1
 * @param x Print font start coordinate of X
 * @param y Print font start coordinate of Y
 * @param X_W Width of BTE Winodwactive window width
 * @param Y_H active window height
 * @param FontColor Set Font Color
 * @param BackGroundColor Set Font BackGround Color 
 * @param tmp2 Hex variable you want print (range : 0 ~ 32bit)
 */
void RA8876_Print_GB12345String(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, char tmp2[]);

/**
 * @brief 
 * 
 * @param Clk SPI CLK = System Clock / 2*(Clk+1)
 * @param SCS 0 : use CS0 , 1 : use CS1
 * @param x Print font start coordinate of X
 * @param y Print font start coordinate of Y
 * @param X_W Width of BTE Winodwactive window width
 * @param Y_H active window height
 * @param FontColor Set Font Color
 * @param BackGroundColor Set Font BackGround Color 
 * @param tmp2 Hex variable you want print (range : 0 ~ 32bit)
 */
void RA8876_Print_UnicodeString(uint8_t Clk, uint8_t SCS, uint16_t x, uint16_t y, uint16_t X_W, uint16_t Y_H, uint32_t FontColor, uint32_t BackGroundColor, uint16_t *tmp2);

/**
 * @brief 
 * 
 * @param Font_Height 	16 : Font = 8x16 �B16x16
			24 : Font = 12x24�B24x24  
			32 : Font = 16x32�B32x32 
 * @param XxN Font Width x 1~4
 * Font Height x 1~4
 * @param ChromaKey 
0 : Font Background color not transparency
1 : Set Font Background color transparency
 * @param Alignment 0 : no alignment, 1 : Set font alignment
 */
void RA8876_Select_Font_Height_WxN_HxN_ChromaKey_Alignment(uint8_t Font_Height, uint8_t XxN, uint8_t YxN, uint8_t ChromaKey, uint8_t Alignment);

/**
 * @brief 
 * 
 * @param str 
 */
void RA8876_Show_String(char *str);

/**
 * @brief 
 * 
 * @param LineColor LineColor : Set Draw Line color. Line Color dataformat :
			ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param X1 X of point1 coordinate
 * @param Y1 Y of point1 coordinate
 * @param X2 X of point2 coordinate
 * @param Y2 Y of point2 coordinate
 */
void RA8876_Draw_Line(uint32_t LineColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param X1 X of point1 coordinate
 * @param Y1 Y of point1 coordinate
 * @param X2 X of point2 coordinate
 * @param Y2 Y of point2 coordinate
 * @param X3 X of point3 coordinate
 * @param Y3 Y of point3 coordinate
 */
void RA8876_Draw_Triangle(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X3, uint16_t Y3);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param X1 X of point1 coordinate
 * @param Y1 Y of point1 coordinate
 * @param X2 X of point2 coordinate
 * @param Y2 Y of point2 coordinate
 * @param X3 X of point3 coordinate
 * @param Y3 Y of point3 coordinate
 */
void RA8876_Draw_Triangle_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t X3, uint16_t Y3);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param R Circle Radius 
 */
void RA8876_Draw_Circle(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param R Circle Radius 
 */
void RA8876_Draw_Circle_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Ellipse(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Ellipse_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Left_Up_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Left_Up_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Right_Down_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Right_Down_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Right_Up_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Right_Up_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Left_Down_Curve(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param XCenter coordinate X of Center 
 * @param YCenter coordinate Y of Center 
 * @param X_R Radius Width of Ellipse 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Left_Down_Curve_Fill(uint32_t ForegroundColor, uint16_t XCenter, uint16_t YCenter, uint16_t X_R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param X1 X of point1 coordinate 
 * @param Y1 Y of point1 coordinate 
 * @param X2 Y of point2 coordinate 
 * @param Y2 Y of point2 coordinate 
 */
void RA8876_Draw_Square(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param X1 X of point1 coordinate 
 * @param Y1 Y of point1 coordinate 
 * @param X2 Y of point2 coordinate 
 * @param Y2 Y of point2 coordinate 
 */
void RA8876_Draw_Square_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param X1 X of point1 coordinate 
 * @param Y1 Y of point1 coordinate 
 * @param X2 Y of point2 coordinate 
 * @param Y2 Y of point2 coordinate 
 * @param R Circle Radius 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Circle_Square(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param ForegroundColor ForegroundColor: Set Draw Triangle color. ForegroundColor Color dataformat :
				ColorDepth_8bpp : R3G3B2�BColorDepth_16bpp : R5G6B5�BColorDepth_24bpp : R8G8B8
 * @param X1 X of point1 coordinate 
 * @param Y1 Y of point1 coordinate 
 * @param X2 Y of point2 coordinate 
 * @param Y2 Y of point2 coordinate 
 * @param R Circle Radius 
 * @param Y_R Radius Length of Ellipse 
 */
void RA8876_Draw_Circle_Square_Fill(uint32_t ForegroundColor, uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t R, uint16_t Y_R);

/**
 * @brief 
 * 
 * @param S0_Addr Start address of Source 0
 * @param S0_W image width of Source 0 (recommend = canvas image width) 
 * @param XS0 coordinate X of Source 0 
 * @param YS0 coordinate Y of Source 0 
 * @param S1_Addr Start address of Source 1
 * @param S1_W image width of Source 1 (recommend = canvas image width)
 * @param XS1 coordinate X of Source 1
 * @param YS1 coordinate Y of Source 1
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param R Circle RadiusOP_Code 
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 */
void RA8876_BTE_Memory_Copy(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H);

/**
 * @brief 
 * 
 * @param S0_Addr Start address of Source 0
 * @param S0_W image width of Source 0 (recommend = canvas image width) 
 * @param XS0 coordinate X of Source 0 
 * @param YS0 coordinate Y of Source 0 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param Background_color The source (1bit map picture) map data 0 translate to background color by color expansion
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 */
void RA8876_BTE_Memory_Copy_Chroma_key(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H);

/**
 * @brief 
 * 
 * @param S1_Addr Start address of Source 1
 * @param S1_W image width of Source 1 (recommend = canvas image width)
 * @param XS1 coordinate X of Source 1
 * @param YS1 coordinate Y of Source 1
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param R Circle RadiusOP_Code 
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param data 
 */
void RA8876_BTE_MCU_Write_MCU_8bit(uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H, const uint8_t *data);

/**
 * @brief 
 * 
 * @param S1_Addr Start address of Source 1
 * @param S1_W image width of Source 1 (recommend = canvas image width)
 * @param XS1 coordinate X of Source 1
 * @param YS1 coordinate Y of Source 1
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param R Circle RadiusOP_Code 
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param data 16-bit data
 */
void RA8876_BTE_MCU_Write_MCU_16bit(uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H, const uint16_t *data);

/**
 * @brief 
 * 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param Background_color The source (1bit map picture) map data 0 translate to background color by color expansion
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param data 8-bit data
 */
void RA8876_BTE_MCU_Write_Chroma_key_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H, const uint8_t *data);

/**
 * @brief 
 * 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param Background_color The source (1bit map picture) map data 0 translate to background color by color expansion
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param data 16-bit data
 */
void RA8876_BTE_MCU_Write_Chroma_key_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Background_color, uint16_t X_W, uint16_t Y_H, const uint16_t *data);

/**
 * @brief 
 * 
 * @param S0_Addr Start address of Source 0
 * @param S0_W image width of Source 0 (recommend = canvas image width) 
 * @param XS0 coordinate X of Source 0 
 * @param YS0 coordinate Y of Source 0 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param Foreground_color The source (1bit map picture) map data 1 translate to Foreground color by color expansion
 * @param Background_color The source (1bit map picture) map data 0 translate to background color by color expansion
 */
void RA8876_BTE_Memory_Copy_ColorExpansion(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color);

/**
 * @brief 
 * 
 * @param S0_Addr Start address of Source 0
 * @param S0_W image width of Source 0 (recommend = canvas image width) 
 * @param XS0 coordinate X of Source 0 
 * @param YS0 coordinate Y of Source 0 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param Foreground_color The source (1bit map picture) map data 1 translate to Foreground color by color expansion
 */
void RA8876_BTE_Memory_Copy_ColorExpansion_Chroma_key(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color);

/**
 * @brief 
 * 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param Foreground_color The source (1bit map picture) map data 1 translate to Foreground color by color expansion
 * @param Background_color The source (1bit map picture) map data 0 translate to background color by color expansion
 * @param data 
 */
void RA8876_BTE_MCU_Write_ColorExpansion_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color, const uint8_t *data);

/**
 * @brief 
 * 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param Foreground_color The source (1bit map picture) map data 1 translate to Foreground color by color expansion
 * @param Background_color The source (1bit map picture) map data 0 translate to background color by color expansion
 * @param data 16-bit data
 */
void RA8876_BTE_MCU_Write_ColorExpansion_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, uint32_t Background_color, const uint16_t *data);

/**
 * @brief 
 * 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param Foreground_color The source (1bit map picture) map data 1 translate to Foreground color by color expansion
 * @param data 8-bit data
 */
void RA8876_BTE_MCU_Write_ColorExpansion_Chroma_key_MCU_8bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, const uint8_t *data);

/**
 * @brief 
 * 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param Foreground_color The source (1bit map picture) map data 1 translate to Foreground color by color expansion
 * @param data 16-bit data
 */
void RA8876_BTE_MCU_Write_ColorExpansion_Chroma_key_MCU_16bit(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint32_t Foreground_color, const uint16_t *data);

/**
 * @brief 
 * 
 * @param P_8x8_or_16x16  0 : use 8x8 Icon , 1 : use 16x16 Icon.
 * @param S0_Addr Start address of Source 0
 * @param S0_W image width of Source 0 (recommend = canvas image width) 
 * @param XS0 coordinate X of Source 0 
 * @param YS0 coordinate Y of Source 0 
 * @param S1_Addr Start address of Source 1
 * @param S1_W image width of Source 1 (recommend = canvas image width)
 * @param XS1 coordinate X of Source 1
 * @param YS1 coordinate Y of Source 1
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param R Circle RadiusOP_Code 
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 */
void RA8876_BTE_Pattern_Fill(uint8_t P_8x8_or_16x16, uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint16_t X_W, uint16_t Y_H);

/**
 * @brief 
 * 
 * @param P_8x8_or_16x16  0 : use 8x8 Icon , 1 : use 16x16 Icon.
 * @param S0_Addr Start address of Source 0
 * @param S0_W image width of Source 0 (recommend = canvas image width) 
 * @param XS0 coordinate X of Source 0 
 * @param YS0 coordinate Y of Source 0 
 * @param S1_Addr Start address of Source 1
 * @param S1_W image width of Source 1 (recommend = canvas image width)
 * @param XS1 coordinate X of Source 1
 * @param YS1 coordinate Y of Source 1
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param R Circle RadiusOP_Code 
 * @param Background_color The source (1bit map picture) map data 0 translate to background color by color expansion
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 */
void RA8876_BTE_Pattern_Fill_With_Chroma_key(uint8_t P_8x8_or_16x16, uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, unsigned int ROP_Code, uint32_t Background_color, uint16_t X_W, uint16_t Y_H);

/**
 * @brief 
 * 
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param Foreground_color The source (1bit map picture) map data 1 translate to Foreground color by color expansion
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 */
void RA8876_BTE_Solid_Fill(uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint32_t Foreground_color, uint16_t X_W, uint16_t Y_H);

/**
 * @brief 
 * 
 * @param S0_Addr Start address of Source 0
 * @param S0_W image width of Source 0 (recommend = canvas image width) 
 * @param XS0 coordinate X of Source 0 
 * @param YS0 coordinate Y of Source 0 
 * @param S1_Addr Start address of Source 1
 * @param S1_W image width of Source 1 (recommend = canvas image width)
 * @param XS1 coordinate X of Source 1
 * @param YS1 coordinate Y of Source 1
 * @param Des_Addr start address of Destination
 * @param Des_W image width of Destination (recommend = canvas image width)
 * @param XDes coordinate X of Destination
 * @param YDes coordinate Y of Destination
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param alpha Alpha Blending effect 0 ~ 32, Destination data = (Source 0 * (1- alpha)) + (Source 1 * alpha)
 */
void RA8876_BTE_Alpha_Blending(uint32_t S0_Addr, uint16_t S0_W, uint16_t XS0, uint16_t YS0, uint32_t S1_Addr, uint16_t S1_W, uint16_t XS1, uint16_t YS1, uint32_t Des_Addr, uint16_t Des_W, uint16_t XDes, uint16_t YDes, uint16_t X_W, uint16_t Y_H, uint8_t alpha);

/**
 * @brief 
 * 
 * @param SCS 0 = Use SCS0, 1 = Use SCS1
 * @param Clk SPI Clock = System Clock /{(Clk+1)*2}
 * @param X1 X of point1 coordinate 
 * @param Y1 Y of point1 coordinate 
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param P_W DMA Picture width 
 * @param Addr DMA Source Start address 
 */
void RA8876_DMA_24bit(uint8_t SCS, uint8_t Clk, uint16_t X1, uint16_t Y1, uint16_t X_W, uint16_t Y_H, uint16_t P_W, uint32_t Addr);

/**
 * @brief 
 * 
 * @param SCS 0 = Use SCS0, 1 = Use SCS1
 * @param Clk SPI Clock = System Clock /{(Clk+1)*2}
 * @param X1 X of point1 coordinate 
 * @param Y1 Y of point1 coordinate 
 * @param X_W Width of BTE Winodw
 * @param Y_H Length of BTE Winodw
 * @param P_W DMA Picture width 
 * @param Addr DMA Source Start address 
 */
void RA8876_DMA_32bit(uint8_t SCS, uint8_t Clk, uint16_t X1, uint16_t Y1, uint16_t X_W, uint16_t Y_H, uint16_t P_W, uint32_t Addr);

/**
 * @brief 
 * 
 * @param SCS 0 = Use SCS0, 1 = Use SCS1
 */
void RA8876_switch_24bits_to_32bits(uint8_t SCS);

/**
 * @brief 
 * 
 * @param on_off on_off = 1 ,enable PWM, on_off = 0 , disable PWM.
 * @param Clock_Divided divided PWM clock, only 0~3(1,1/2,1/4,1/8) 
 * @param Prescalar 
 * @param Count_Buffer Count_Buffer : set PWM output period time
 * @param Compare_Buffer Compare_Buffer : set PWM output high level time(Duty cycle) 
 Such as the following formula :
PWM CLK = (Core CLK / Prescalar ) /2^ divided clock 
PWM output period = (Count Buffer + 1) x PWM CLK time
PWM output high level time = (Compare Buffer + 1) x PWM CLK time 
 */
void RA8876_PWM0(uint8_t on_off, uint8_t Clock_Divided, uint8_t Prescalar, uint16_t Count_Buffer, uint16_t Compare_Buffer);

/**
 * @brief 
 * 
 * @param on_off on_off = 1 ,enable PWM, on_off = 0 , disable PWM.
 * @param Clock_Divided divided PWM clock, only 0~3(1,1/2,1/4,1/8) 
 * @param Prescalar 
 * @param Count_Buffer Count_Buffer : set PWM output period time
 * @param Compare_Buffer Compare_Buffer : set PWM output high level time(Duty cycle) 
 Such as the following formula :
PWM CLK = (Core CLK / Prescalar ) /2^ divided clock 
PWM output period = (Count Buffer + 1) x PWM CLK time
PWM output high level time = (Compare Buffer + 1) x PWM CLK time 
 */
void RA8876_PWM1(uint8_t on_off, uint8_t Clock_Divided, uint8_t Prescalar, uint16_t Count_Buffer, uint16_t Compare_Buffer);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param color color : 
8bpp:R3G3B2
16bpp:R5G6B5
24bpp:R8G8B8
 */
void RA8876_putPixel(uint16_t x, uint16_t y, uint32_t color);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param fgcolor foreground color(font color)
 * @param bgcolor background color
8bpp:R3G3B2
16bpp:R5G6B5
24bpp:R8G8B8
 * @param bg_transparent 
bg_transparent = 0, background color with no transparent
bg_transparent = 1, background color with transparent
 * @param code font char
 */
void RA8876_lcdPutChar8x12(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param fgcolor foreground color(font color)
 * @param bgcolor background color
8bpp:R3G3B2
16bpp:R5G6B5
24bpp:R8G8B8
 * @param bg_transparent 
bg_transparent = 0, background color with no transparent
bg_transparent = 1, background color with transparent
 * @param ptr font string
 */
void RA8876_lcdPutString8x12(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param fgcolor foreground color(font color)
 * @param bgcolor background color
8bpp:R3G3B2
16bpp:R5G6B5
24bpp:R8G8B8
 * @param bg_transparent 
bg_transparent = 0, background color with no transparent
bg_transparent = 1, background color with transparent
 * @param code font char
 */
void RA8876_lcdPutChar16x24(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param fgcolor foreground color(font color)
 * @param bgcolor background color
8bpp:R3G3B2
16bpp:R5G6B5
24bpp:R8G8B8
 * @param bg_transparent 
bg_transparent = 0, background color with no transparent
bg_transparent = 1, background color with transparent
 * @param ptr font string
 */
void RA8876_lcdPutString16x24(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param fgcolor foreground color(font color)
 * @param bgcolor background color
8bpp:R3G3B2
16bpp:R5G6B5
24bpp:R8G8B8
 * @param bg_transparent 
bg_transparent = 0, background color with no transparent
bg_transparent = 1, background color with transparent
 * @param code font char
 */
void RA8876_lcdPutChar32x48(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, uint8_t code);

/**
 * @brief 
 * 
 * @param x x of coordinate
 * @param y y of coordinate
 * @param fgcolor foreground color(font color)
 * @param bgcolor background color
8bpp:R3G3B2
16bpp:R5G6B5
24bpp:R8G8B8
 * @param bg_transparent 
bg_transparent = 0, background color with no transparent
bg_transparent = 1, background color with transparent
 * @param ptr font string
 */
void RA8876_lcdPutString32x48(uint16_t x, uint16_t y, uint32_t fgcolor, uint32_t bgcolor, uint8_t bg_transparent, char *ptr);
