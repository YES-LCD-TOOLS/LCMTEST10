/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file   fatfs.c
 * @brief  Code for fatfs applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
#include "fatfs.h"

uint8_t retSD;    /* Return value for SD */
char SDPath[4];   /* SD logical drive path */
FATFS SDFatFS;    /* File system object for SD logical drive */
FIL SDFile;       /* File object for SD */

/* USER CODE BEGIN Variables */

/* USER CODE END Variables */

void MX_FATFS_Init(void)
{
  /*## FatFS: Link the SD driver ###########################*/
  retSD = FATFS_LinkDriver(&SD_Driver, SDPath);

  /* USER CODE BEGIN Init */
	/* additional user code for init */
  /* USER CODE END Init */
}

/**
  * @brief  Gets Time from RTC
  * @param  None
  * @retval Time in DWORD
  */
DWORD get_fattime(void)
{
  /* USER CODE BEGIN get_fattime */
	return 0;
  /* USER CODE END get_fattime */
}

/* USER CODE BEGIN Application */

void fs_check_sdcard(FATFS *fs)
{
	int result = 0;
	FIL fil;
	FRESULT res;

	//
	uint8_t wtext[] = "SD Check: Read from write file SUCCESS\r\n";
	uint8_t rtext[100];
	uint32_t byteswritten;
	uint32_t bytesread;
	const char test_file_name[] = "FatFs_Test.txt";

	//
	res = f_mount(fs, "", 0);
	if (res) {
		printf("SD Check f_mount error %d\r\n", res);
		result += 1;
	}

	//
	res = f_open(&fil, test_file_name, FA_OPEN_ALWAYS | FA_WRITE);
	if (res) {
		printf("SD Check f_open error %d\r\n", res);
		result += 1;
	}

	res = f_write(&fil, wtext, sizeof(wtext), (void *)&byteswritten);
	if (res) {
		printf("SD Check f_write error %d\r\n", res);
		result += 1;
	}

	//
	res = f_close(&fil);
	if (res) {
		printf("SD Check f_close error %d\r\n", res);
		result += 1;
	}

	//
	res = f_open(&fil, test_file_name, FA_READ | FA_OPEN_EXISTING);
	if (res != FR_OK) {
		printf("SD Check f_open error %d\r\n", res);
		result += 1;
	}

	//
	res = f_read(&fil, rtext, 54, (UINT *)&bytesread);
	if (res != FR_OK) {
		printf("SD Check read error %d\r\n", res);
		result += 1;
	}

	//
	res = f_close(&fil);
	if (res) {
		printf("SD Check f_close error %d\r\n", res);
		result += 1;
	}

	// remove testing file
	res = f_unlink(test_file_name);
	if (res) {
		printf("SD Check f_unlink error %d\r\n", res);
		result += 1;
	}

	if (result != 0) {
		printf("SDcard check failed\r\n");
		printf("SD Check write %d bytes, read %d bytes\r\n",
		       (int)byteswritten, (int)bytesread);
		printf((const char *)rtext);

		Error_Handler();
	} else {
		printf("[SDcard] Check Success\r\n");
	}

	return;
}
/* USER CODE END Application */
